# Globe Tracker API Spec

# Work in Progress (2017-04-03)

This is a work in progress and an attempt to use the Swagger API Spec
as a tool for quickly communicating the needs for the mobile app API.

# Running Swagger UI

The Swagger UI requires it be run from an http server when the spec
is not publically accessible. To run locally, you can use `http-server`:

```
sudo npm -g install http-server
```
and then from the api/swagger directory:

```
http-server --cors
```
Now open this URL in your browser:

http://127.0.0.1:8080/?url=/GT_API_Spec.yaml
