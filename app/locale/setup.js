import I18n from 'react-native-i18n'

I18n.fallbacks = true

// Init these as objects/maps
I18n.translations.en = {
  common: {
    supportEmail: 'support@globetracker.com',
    next: 'Next',
    back: 'Back',
    scan: 'Scan',
    required: 'Required'
  }
}
I18n.translations.es = {
  common: {
    supportEmail: 'support@globetracker.com',
    next: 'Siguiente',
    scan: 'Escanear',
    back: 'Anterior',
    required: 'Necesario'
  }
}

export default I18n
