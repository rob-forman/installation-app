import { call, all, takeEvery, takeLatest, select, put } from 'redux-saga/effects'
import { AsyncStorage } from 'react-native'
import RNFetchBlob from 'react-native-fetch-blob'
import RNFS from 'react-native-fs'
import { types } from './actions'
import { types as authTypes } from '../auth/actions'
import { types as installsTypes } from '../installs/actions'
import { extractImageUrisFromInstallCommands } from '../installs/utils'
import { imagesDirectory } from '../common/constants'
import { imageUrl } from '../auth/api'

const StorageKeys = {
  images: 'storage.images'
}

const uploadImage = (imageUri /*, accessToken*/) => {

  const index = imageUri.lastIndexOf('/')
  let filename
  if (index != -1) {
    filename = imageUri.substring(index + 1)
  } else {
    filename = imageUri
  }

  if (imageUri.startsWith('file://')) {
    imageUri = imageUri.substring('file://'.length)
  }

  const body = [{
    name: 'image',
    filename: filename,
    type:'image/jpeg',
    data: RNFetchBlob.wrap(imageUri)
  }]

  return RNFetchBlob.fetch('POST', imageUrl, {
    'Authorization': `Bearer ${0}`,
    'Content-Type': 'multipart/form-data',
  }, body)
  .then(res => {
    console.log('uploadImage res', res)
    const text = res.text()
    console.log(text)
    return text
  })
  .catch(error => {
    console.error('uploadImage', error.message, imageUri)
    throw error
  })
}

const filterImages = (images) => {
  return RNFS.readDir(imagesDirectory)
  .then(content => {
    const newImages = {}
    for (const file of content) {
      const imageUri = `file://${file.path}`
      const image = images[imageUri]
      if (image) {
        newImages[imageUri] = image
      }
    }
    return newImages
  })
  .catch(error => {
    console.log(error)
    return {}
  })
}

function* isUploadingAnyImage() {
  return !!(yield select(state => (state.dataUpload.imageUriToUpload)))
}

function* tryToUploadImage(images, previousImageUri = null) {
  if (yield isUploadingAnyImage()) {
    return null
  }

  const imagesUris = Object.keys(images)
  const imagesToUploadCount = imagesUris.reduce((count, uri) => (
    images[uri] && !images[uri].isUploaded ? count + 1 : count
  ), 0)

  if (imagesToUploadCount == 0) {
    // No image to upload
    return
  }

  let index = imagesUris.indexOf(previousImageUri) + 1

  let imageUriToUpload = null

  for (let i = index; i < imagesUris.length; i++) {
    const uri = imagesUris[i]
    const image = images[uri]
    if (!image.isUploaded) {
      imageUriToUpload = uri
      break
    }
  }

  if (imageUriToUpload) {
    yield put({
      type: types.UPLOAD_IMAGE,
      value: imageUriToUpload
    })
  }
}

function *onUploadImage({ value: imageUri }) {
  try {
    const accessToken = yield select(state => (state.auth.authenticatedUser.accessToken))
    yield call(uploadImage, imageUri, accessToken)
    yield put({
      type: types.UPLOAD_IMAGE_SUCCESS,
      value: imageUri
    })
  } catch (error) {
    yield put({
      type: types.UPLOAD_IMAGE_FAILURE,
      value: imageUri
    })
  }
}

function *onImageUploadFinished({ value: imageUri }) {
  const images = yield select(state => (state.dataUpload.images))
  yield storeImages(images)
  yield tryToUploadImage(images, imageUri)
}

function* getStoredImages() {
  const json = yield call(AsyncStorage.getItem, StorageKeys.images)
  if (json) {
    return JSON.parse(json)
  } else {
    return {}
  }
}

function* storeImages(images) {
  yield call(AsyncStorage.setItem, StorageKeys.images, JSON.stringify(images))
}

function* onUserLoaded() {
  let images = yield getStoredImages()
  images = yield call(filterImages, images)
  yield put({
    type: types.ADD_IMAGES,
    value: images
  })
}

function* onAddImages() {
  const images = yield select(state => (state.dataUpload.images))
  yield storeImages(images)
  yield tryToUploadImage(images)
}

function* onSetImages({ value: images }) {
  yield storeImages(images)
  yield tryToUploadImage(images)
}

function* onCompleteInstall() {
  const { commands, images } = yield select(state => ({
    commands: state.installs.installCommands,
    images: state.dataUpload.images
  }))

  const newImages = {}

  for (const uri of extractImageUrisFromInstallCommands(commands)) {
    newImages[uri] = images[uri] || {
      isUploaded: false
    }
  }

  yield put({
    type: types.SET_IMAGES,
    value: newImages
  })
}

export default function* dataUploadSaga() {
  yield all([
    takeLatest(authTypes.LOGIN_SUCCESS, onUserLoaded),
    takeLatest(types.UPLOAD_IMAGE, onUploadImage),
    takeEvery([types.UPLOAD_IMAGE_SUCCESS, types.UPLOAD_IMAGE_FAILURE], onImageUploadFinished),
    takeLatest(types.ADD_IMAGES, onAddImages),
    takeLatest(types.SET_IMAGES, onSetImages),
    takeLatest([installsTypes.FORM_COMPLETE_INSTALL, installsTypes.FORCE_UPLOAD_ALL], onCompleteInstall),
  ])
}
