import { types } from './actions'

const initialState = {
  imageUriToUpload: null,
  images: {
    // [imageUri]: { isUploaded: false }
  },
}

const reduceImage = (state, uri, imageReducer) => {
  const image = state.images[uri]
  if (!image) {
    return state
  } else {
    const { isUploading, ...image } = imageReducer(image)
    return {
      ...state,
      imageUriToUpload: isUploading ? uri : (state.imageUriToUpload == uri ? null : state.imageToUpload),
      images: {
        ...state.images,
        [uri]: image
      }
    }
  }
}

export default (state = initialState, { type, value }) => {
  switch (type) {
  case types.UPLOAD_IMAGE:
    return reduceImage(state, value, image => ({
      ...image,
    }))

  case types.UPLOAD_IMAGE_SUCCESS:
    return reduceImage(state, value, image => ({
      ...image,
      isUploading: false,
      isUploaded: true
    }))

  case types.UPLOAD_IMAGE_FAILURE:
    return reduceImage(state, value, image => ({
      ...image,
      isUploading: false,
      isUploaded: false
    }))

  case types.ADD_IMAGES:
    return {
      ...state,
      images: {
        ...state.images,
        ...value
      }
    }

  case types.SET_IMAGES:
    return {
      ...state,
      imageUriToUpload: state.imageUriToUpload in value ? state.imageUriToUpload : null,
      images: value
    }

  default:
    return state
  }
}
