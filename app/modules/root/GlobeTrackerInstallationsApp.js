import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './rootSaga'

import { logger } from 'redux-logger'
import rootReducer from './reducer'
import {
  AppRegistry,
  Platform
} from 'react-native'
import SplashScreen from 'react-native-smart-splash-screen'
import RootScreen from './RootScreen'

const sagaMiddleware = createSagaMiddleware()
let store = createStore(rootReducer, {}, applyMiddleware(sagaMiddleware, thunk, logger))
sagaMiddleware.run(rootSaga)

const initialState = {
  dbIsReady: true
}

export default class GlobeTrackerInstallationsApp extends Component {
  constructor(props) {
    super(props)
    this.state = initialState
  }

  componentDidMount() {
    if (Platform.OS == 'android') {
      SplashScreen.close({
        animationType: SplashScreen.animationType.scale,
        duration: 500,
        delay: 0,
      })
    }
  }

  render() {
    if (this.state.dbIsReady) {
      return (
        <Provider store={store}>
          <RootScreen />
        </Provider>
      )
    } else {
      return null
    }
  }
}

AppRegistry.registerComponent('GlobeTrackerInstallationsApp', () => GlobeTrackerInstallationsApp)
