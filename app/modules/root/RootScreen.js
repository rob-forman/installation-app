import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import LoginScreen from '../auth/LoginScreen'
import RootNavigator from './RootNavigator'
import actions from '../auth/actions'

class RootScreenComponent extends React.Component {

  componentDidMount() {
    if (!this.props.auth.isBooting) {
      this.props.actions.boot()
    }
  }

  render() {
    if (this.props.auth.authenticatedUser) {
      return <RootNavigator />
    } else {
      return <LoginScreen />
    }
  }
}

export default connect(
  state => ({
    auth: state.auth
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(RootScreenComponent)
