import React from 'react'
import { DrawerNavigator, StackNavigator } from 'react-navigation'
import { View, Text } from 'react-native'
import BUILD_NUMBER from '../BUILD_NUMBER'

import { LogoDrawer } from '../common/CommonUI'
import { DrawerItems } from 'react-navigation'
import LogoutScreen from '../auth/LogoutScreen'
import InstallScreen from '../installs/InstallScreen'
import InstallHomeScreen from '../installs/InstallHomeScreen'
import InstallListScreen from '../installs/InstallListScreen'
import InstallDetailScreen from '../installs/InstallDetailScreen'
import InstallSensorScreen from '../installs/InstallSensorScreen'
import GuidelineCategoriesScreen from '../guidelines/GuidelineCategoriesScreen'
import GuidelineManufacturersScreen from '../guidelines/GuidelineManufacturersScreen'
import GuidelineDetailScreen from '../guidelines/GuidelineDetailScreen'
import ZoomableScreen from '../common/ZoomableScreen'
import ScanBarcodeScreen from '../installs/ScanBarcodeScreen'
import CaptureScreen from '../installs/CaptureScreen'
import InstallGuidelinesScreen from '../installs/InstallGuidelinesScreen'

const InstallListStack = StackNavigator({
  InstallList: { screen: InstallListScreen },
  Install: { screen: InstallScreen },
}, {
  headerMode: 'none',
  initialRouteName: 'InstallList'
})

const InstallStack = StackNavigator({
  Install: { screen: InstallScreen },
  InstallList: { screen: InstallListStack },
  InstallHome: { screen: InstallHomeScreen },
  InstallDetail: { screen: InstallDetailScreen },
  InstallSensor: { screen: InstallSensorScreen },
  InstallGuidelines: { screen: InstallGuidelinesScreen },
  GuidelineManufacturers: { screen: GuidelineManufacturersScreen },
  GuidelineDetail: { screen: GuidelineDetailScreen },
  ZoomableScreen: { screen: ZoomableScreen },
  CaptureScreen: { screen: CaptureScreen },
  ScanBarcode: { screen: ScanBarcodeScreen }
}, {
  headerMode: 'screen',
  initialRouteName: 'InstallHome'
})

const GuidelineStack = StackNavigator({
  GuidelineCategories: { screen: GuidelineCategoriesScreen },
  GuidelineManufacturers: { screen: GuidelineManufacturersScreen },
  GuidelineDetail: { screen: GuidelineDetailScreen },
  ZoomableScreen: { screen: ZoomableScreen }
}, {
  headerMode: 'screen',
  initialRouteName: 'GuidelineCategories'
})

const DrawerComponent = (props) => (
  <View style={{ flex: 1 }}>
    <LogoDrawer />
    <View style={{margin: 20}}></View>
    <DrawerItems {...props} />

    <View style={{position: 'absolute', bottom: 0, margin: 20}}>
    <Text>{BUILD_NUMBER}</Text>
    </View>
  </View>
)

export default DrawerNavigator({
  Install: { screen: InstallStack },
  // Test: { screen: TestStack },
  Guidelines: { screen: GuidelineStack },
  Logout: { screen: LogoutScreen }
}, {
  contentComponent: DrawerComponent,
  initialRouteName: 'Install'
})
