import { combineReducers } from 'redux'
import auth from '../auth/reducer'
import dataFetch from '../dataFetch/reducer'
import dataUpload from '../dataUpload/reducer'
import installs from '../installs/reducer'

const reducer = combineReducers({
  auth,
  dataFetch,
  dataUpload,
  installs
})

export default reducer
