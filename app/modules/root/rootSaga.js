import { all, fork } from 'redux-saga/effects'

import authSagas from '../auth/sagas'
import dataUploadSaga from '../dataUpload/saga'
import installSaga from '../installs/sagas'

export default function* root() {
  yield all([
    fork(authSagas),
    fork(dataUploadSaga),
    fork(installSaga)
  ])
}
