import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { Button } from '../common/ui/Buttons'
import { BASELINE } from '../common/ui/constants'
import I18n from './locale'

import GuidelinesData from './constants/guidelines'

class GuidelineManufacturersScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: I18n.t(`guidelines.titles.${navigation.state.params}`),
  })

  navigateToDetail = (key) => () => this.props.navigation.navigate('GuidelineDetail', {
    category: this.props.navigation.state.params,
    manufacturer: key
  })

  render() {
    const selectedCategory = this.props.navigation.state.params

    return (
      <ScrollView contentContainerStyle={styles.container}>
        { GuidelinesData[selectedCategory].map(item =>
          <Button
            block
            key={item.unitId}
            style={styles.button}
            onPress={this.navigateToDetail(item.unitId)}
          >
            {`${selectedCategory === 'commUnits' ? 'CU' : 'S'}: ${item.unitId}`}
          </Button>
        )}
      </ScrollView>
    )
  }
}

export default GuidelineManufacturersScreen

const styles = {
  container: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10%',
    paddingTop: '5%',
  },
  button: {
    marginBottom: BASELINE / 2,
  }
}
