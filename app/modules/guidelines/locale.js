import I18n from '../../locale/setup'

I18n.translations.en.guidelines = {
  navTitle: 'Guidelines',
  detail: 'Guideline Detail',
  noGuidelines: 'No Guidelines yet.',
  noGuideline: 'No Guideline for selected manufacturer.'
}

I18n.translations.en.guidelines.keys = {
  commUnits: 'Comm Unit Guidelines',
  sensors: 'Sensor Guidelines'
}

I18n.translations.en.guidelines.titles = {
  commUnits: 'CU Guidelines',
  sensors: 'Sensor Guidelines'
}

I18n.translations.es.guidelines = {
  navTitle: 'Procedimientos',
  detail: 'Detalle de las procedimientos',
  noGuidelines: 'No procedimientos.',
  noGuideline: 'No procedimientos para el fabricante seleccionado.'
}

I18n.translations.es.guidelines.keys = {
  commUnits: 'UC Procedimientos',
  sensors: 'Sensor Procedimientos'
}

I18n.translations.es.guidelines.titles = {
  commUnits: 'UC Procedimientos',
  sensors: 'Procedimientos del sensor'
}


export default I18n
