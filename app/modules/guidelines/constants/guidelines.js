export default {
  commUnits: [
    {
      unitId: 'TK',
      text: 'Installation of Comm Unit on TK container',
      image: require('../../../../assets/img/1.jpg')
    },
    {
      unitId: 'Daikin',
      text: 'Installation of Comm Unit on Daikin container',
      image: require('../../../../assets/img/2.jpg')
    },
    {
      unitId: 'Starcool',
      text: 'Installation of Comm Unit on Starcool container',
      image: require('../../../../assets/img/3.jpg')
    },
    {
      unitId: 'Carrier',
      text: 'Installation of Comm Unit on Carrier container',
      image: require('../../../../assets/img/4.jpg')
    }
  ],
  sensors: [
    {
      unitId: 'TK',
      text: 'Installation of sensor on TK container',
      image: require('../../../../assets/img/5.jpg')
    },
    {
      unitId: 'Daikin',
      text: 'Installation of sensor on Daikin container',
      image: require('../../../../assets/img/6.jpg')
    },
    {
      unitId: 'Starcool',
      text: 'Installation of sensor on Starcool container',
      image: require('../../../../assets/img/7.jpg')
    },
    {
      unitId: 'Carrier',
      text: 'Installation of Comm Unit on Carrier container',
      image: require('../../../../assets/img/8.jpg')
    }
  ]
}
