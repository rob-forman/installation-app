import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { DrawerHeaderLeft } from '../common/CommonUI'
import { Button } from '../common/ui/Buttons'
import { BASELINE } from '../common/ui/constants'
import I18n from './locale'
import { COLOR_BLUE, COLOR_WHITE, COLOR_LIGHT_GREY_BLUE } from '../common/ui/constants'

import GuidelinesData from './constants/guidelines'

class GuidelineCategoriesScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('guidelines.navTitle'),
    headerStyle: {
    backgroundColor: COLOR_BLUE,
    },
    headerTintColor: '#fff',
    headerLeft: <DrawerHeaderLeft navigation={navigation} />
  })
    
  navigateToSublist = (key) => () => this.props.navigation.navigate('GuidelineManufacturers', key)

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        { Object.keys(GuidelinesData).map(key =>
          <Button
            block
            key={key}
            style={styles.button}
            onPress={this.navigateToSublist(key)}
          >{I18n.t(`guidelines.keys.${key}`)}</Button>
        )}
      </ScrollView>
    )
  }
}

export default GuidelineCategoriesScreen

const styles = {
  container: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10%',
    paddingTop: '5%',
    backgroundColor: COLOR_LIGHT_GREY_BLUE,
  },
  button: {
    marginBottom: BASELINE / 2,
  }
}
