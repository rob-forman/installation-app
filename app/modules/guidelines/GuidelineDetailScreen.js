import React, { Component } from 'react'
import {
  StyleSheet,
  ScrollView,
  Text
} from 'react-native'
import { ThumbnailView } from '../common/CommonUI'
import I18n from './locale'

import GuidelinesData from './constants/guidelines'

class GuidelineDetailScreen extends Component {

  static navigationOptions = () => ({
    title: I18n.t('guidelines.detail')
  })

  navigateToZoomable = (image) => () => this.props.navigation.navigate('ZoomableScreen', image)

  render() {
    const { category, manufacturer } = this.props.navigation.state.params
    const guideline = GuidelinesData[category]
      .find(item => item.unitId === manufacturer)

    if (!guideline) {
      return (
        <ScrollView contentContainerStyle={styles.container}>
          <Text>{I18n.t('guideline.noGuideline')}</Text>
        </ScrollView>
      )
    }
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <ThumbnailView
          onPress={this.navigateToZoomable}
          text={guideline.text} image={guideline.image}>
        </ThumbnailView>
      </ScrollView>
    )
  }
}

export default GuidelineDetailScreen

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10%',
    paddingTop: '5%',
  }
})
