

const ns = 'GUIDELINES'
export const types = {
  RESET: ns+'/RESET',

}

const actions = {
  reset: () => ({type: types.RESET})
}

export default actions
