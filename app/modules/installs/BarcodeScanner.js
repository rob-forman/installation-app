import React from 'react'
import { View, StyleSheet } from 'react-native'
import Camera from 'react-native-camera'

const BarcodeScanner = props => (
  <Camera
    style={styles.camera}
    aspect={Camera.constants.Aspect.fill}
    orientation={Camera.constants.Orientation.portrait}
    onBarCodeRead={props.onRead}>
    <View style={styles.barcodeOuter}>
      <View style={styles.barcodeInner}>
      </View>
    </View>
  </Camera>
)

export default BarcodeScanner

const styles = StyleSheet.create({
  camera: {
    height: '100%',
    width: '100%'
  },
  barcodeOuter: {
    flex: 1,
    height: '100%',
    width: '100%',
    padding: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  barcodeInner: {
    height: '50%',
    width: '100%',
    borderColor: '#00ff00',
    borderWidth: 2,
    borderRadius: 2
  }
})
