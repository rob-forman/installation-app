import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import actions from './actions'
import CameraWrapper from './CameraWrapper'
import I18n from './locale'

export const CaptureTargetTypes = {
  comm: 'comm',
  asset: 'asset',
  sensor: 'sensor'
}

class CaptureComponent extends Component {

  onPictureTaken = (data) => {
    const { navigation, onPictureTaken } = this.props
    navigation.goBack()
    onPictureTaken(data.uri)
  }

  static navigationOptions = ({ navigation }) => ({
    title: (() => {
      switch (navigation.state.params) {
      case CaptureTargetTypes.comm: return I18n.t('camera.commUnitPicture')
      case CaptureTargetTypes.asset: return I18n.t('camera.installationPicture')
      case CaptureTargetTypes.sensor: return I18n.t('camera.sensorPicture')
      }
    })()
  })

  render() {
    return <CameraWrapper onPictureTaken={this.onPictureTaken} />
  }
}

export default connect(
  (state) => ({
    assetImage: state.installs.current.asset.picture,
    commUnitImage: state.installs.current.comm.picture
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  }),
  (stateProps, dispatchProps, ownProps) => {
    const type = ownProps.navigation.state.params
    switch(type) {
    case CaptureTargetTypes.comm:
      return ({
        ...ownProps,
        currentImage: stateProps.commUnitImage,
        onPictureTaken: dispatchProps.actions.setCurrentCommUnitPicture
      })
    case CaptureTargetTypes.asset:
      return ({
        ...ownProps,
        currentImage: stateProps.assetImage,
        onPictureTaken:  dispatchProps.actions.setCurrentAssetPicture
      })
    case CaptureTargetTypes.sensor:
      return ({
        ...ownProps,
        currentImage: null,
        onPictureTaken:  dispatchProps.actions.formSensorPicture
      })
    }
  }
)(CaptureComponent)
