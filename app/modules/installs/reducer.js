import { types } from './actions'
import { reduceFormField } from '../../tools/reducers'
import { generateId } from '../common/utils'

const initialState = {
  isUploading: false,
  isPending: false,
  isGuidelineVisible: false,
  form: { // TODO remove *form*, put everything in *current* and transform it via reselect
    companyId: { value: null, error: null },
    assetId: { value: null, error: null },
    assetType: {value: null, error: null},
    commId: { value: null, error: null },
    sensorId: { value: null, error: null },
    sensorTypes: {},
    sensorLocation: { value: null, error: null },
    sensorPictures: [],
    visitedPages: [],
  },
  current: {
    customFields: {
      manufacturer: null,
      containerType: null,
      containerSize: null,
      certification: null,
      notes: null
    },
    company: { id: null, name: '' },
    asset: {
      id: null,
      attributes: {},
      picture: null
    },
    comm: {
      id: null,
      picture: null,
      placement: null
    },
    sensors: [
      // {id: null, type: [], location: '...' }
    ],
  },


  // [{
  //   install: state.current,
  //   date: Date.now(),
  //   isUploaded: false
  // }]
  installCommands: []
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.RESET:
    return initialState
  case types.RESET_FORM:
    return {
      ...state,
      form: initialState.form
    }
  case types.RESET_CURRENT:
    return {
      ...state,
      current: initialState.current
    }
  case types.FORM_COMPANY_ID_PENDING:
    return reduceFormField('companyId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_COMPANY_ID_SUCCESS:
    return reduceFormField('companyId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_COMPANY_ID_FAILURE:
    return {
      ...state,
      ...reduceFormField('companyId', state, {
        value: action.value,
        error: action.error,
      }),
    }

  case types.SET_CURRENT_COMPANY:
    return {
      ...state,
      current: { ...state.current, company: action.value },
    }

  case types.SET_ASSET_TYPE:
    return reduceFormField('assetType', state, {
      value: action.value
    })
  case types.FORM_ASSET_ID_PENDING:
    return reduceFormField('assetId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_ASSET_ID_SUCCESS:
    return reduceFormField('assetId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_ASSET_ID_FAILURE:
    return {
      ...state,
      ...reduceFormField('assetId', state, {
        value: action.value,
        error: action.error,
      }),
    }

  case types.FORM_ASSET_TYPE_FAILURE:
    return reduceFormField('assetType', state, {
      value: action.value,
      error: action.error
    })
  case types.FORM_ASSET_TYPE_SUCCESS:
    return reduceFormField('assetType', state, {
      value: action.value,
      error: null,
    })

  case types.SET_CURRENT_ASSET:
    return {
      ...state,
      current: {
        ...state.current,
        asset: action.value
      },
    }
  case types.FORM_COMM_PLACEMENT:
    return {
      ...state,
      current: {
        ...state.current,
        comm: {
          ...state.current.comm,
          placement: action.value
        }
      }
    }
  case types.FORM_COMM_ID_PENDING:
    return reduceFormField('commId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_COMM_ID_SUCCESS:
    return reduceFormField('commId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_COMM_ID_FAILURE:
    return {
      ...state,
      ...reduceFormField('commId', state, {
        value: action.value,
        error: action.error,
      }),
    }

  case types.SET_CURRENT_COMM:
    return {
      ...state,
      current: {
        ...state.current,
        comm: {
          ...state.current.comm,
          ...action.value
        }
      },
    }

  // Form sensors
  case types.FORM_SENSOR_ID_SUCCESS:
    return reduceFormField('sensorId', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_SENSOR_ID_FAILURE:
    return reduceFormField('sensorId', state, {
      value: action.value,
      error: action.error,
    })


  case types.FORM_SENSOR_TYPE_PENDING:
    return reduceFormField('sensorTypes', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_SENSOR_TYPE_CHECKED:
    return reduceFormField('sensorTypes', state, {
      [action.value]: true
    })
  case types.FORM_SENSOR_TYPE_UNCHECKED:
    return reduceFormField('sensorTypes', state, {
      [action.value]: false
    })
  case types.FORM_SENSOR_LOCATION_SUCCESS:
    return reduceFormField('sensorLocation', state, {
      value: action.value,
      error: null,
    })

  case types.SET_CURRENT_COMM_UNIT_PICTURE_SUCCESS:
    return {
      ...state,
      current: {
        ...state.current,
        comm: {
          ...state.current.comm,
          picture: action.value
        }
      }
    }

  case types.SET_CURRENT_ASSET_PICTURE_SUCCESS:
    return {
      ...state,
      current: {
        ...state.current,
        asset: {
          ...state.current.asset,
          picture: action.value
        }
      }
    }

  case types.FORM_SENSOR_PICTURE_SUCCESS:
    return {
      ...state,
      form: {
        ...state.form,
        sensorPictures: [
          ...state.form.sensorPictures,
          action.value
        ]
      }
    }

  case types.DELETE_SENSOR:
    return {
      ...state,
      current: {
        ...state.current,
        sensors: state.current.sensors.filter((sensor) => (sensor !== action.value))
      }
    }

  case types.RESET_SENSOR:
    return {
      ...state,
      form: {
        ...state.form,
        sensorId: {value: null, error: null},
        sensorLocation: {value: null, error: null},
        sensorTypes: {},
        sensorPictures: []
      }
    }

  case types.SET_CURRENT_SENSOR:
    return {
      ...state,
      current: {
        ...state.current,
        sensors: [...state.current.sensors, action.value]
      },
      form: {
        ...state.form,
        sensorPictures: []
      }
    }

  case types.FORM_COMPLETE_INSTALL:
    return {
      ...state,
      installCommands: [
        ...state.installCommands.filter(item => item.install.id != state.current.id), {
          install: {
            ...state.current,
            // Client's API does not accept id as a string
            id: state.current.id || generateId(),
          },
          form: state.form,
          date: Date.now(),
          isUploaded: false,
        }
      ],
      current: {...initialState.current},
      form: initialState.form
    }

  // guidelines
  case types.GUIDELINE_VISIBLE:
    return { ...state, isGuidelineVisible: true }

  case types.GUIDELINE_HIDDEN:
    return { ...state, isGuidelineVisible: false }

  // existing installs
  case types.LOAD_INSTALL:
    return {
      ...state,
      current: action.value.current,
      form: action.value.form
    }

  case types.SET_INSTALLS_FROM_LOCAL_STORAGE:
    return {
      ...state,
      installCommands: action.value
    }

  case types.VISIT_PAGE: {
    const visitedPages = state.form.visitedPages.slice()
    visitedPages[action.value] = true
    return {
      ...state,
      form: {
        ...state.form,
        visitedPages
      }
    }
  }

  case types.UPLOADING_IN_PROGRESS:
    return {
      ...state,
      isUploading: action.value
    }

  case types.FORM_CUSTOM_FIELD_CHANGED:
    return {
      ...state,
      current: {
        ...state.current,
        customFields: {
          ...state.current.customFields,
          [action.value.field]: action.value.value
        }
      }
    }
  default:
    return state
  }
}
