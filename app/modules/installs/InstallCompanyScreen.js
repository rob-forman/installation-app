import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { View, Text } from 'react-native'
import { Link } from '../common/CommonUI'
import { Select, Option } from '../common/ui'
import I18n from './locale'
import actions from './actions'
import styles from './styles'

import { Container, Content } from 'native-base'

class InstallCompanyScreenComponent extends Component {

  render() {
    const defaultProjectText = I18n.t('installs.defaultProject')
    const projects = [
      {id: 1, name: defaultProjectText},
      // { id: 1, name: 'CM Only' },
      // { id: 2, name: 'CM + 1 Sensor' },
      // { id: 3, name: 'CM + 2 Sensors' },
      // { id: 4, name: 'Per Trip' },
      // { id: 5, name: 'Chassis' },
      // { id: 6, name: 'Dry' },
    ]
    return (
      <Container style={styles.page}>
        <Content style={styles.content}>
          <Select
            label={I18n.t('installs.company')}
            placeholder={I18n.t('installs.selectCompany')}
            selectedValue={this.props.companyId.value}
            error={this.props.companyId.error}
            onValueChange={this.props.actions.formCompany}
          >
            {this.props.companies.map(c => (
              <Option key={c.id} label={c.name} value={c.id} />
            ))}
          </Select>
            <View style={{height: 50}} />
          <Select
            label={I18n.t('installs.project')}
            placeholder={defaultProjectText}
            selectedValue={1}
            onValueChange={value => console.log(value)}
          >
            {projects.map(p => (
              <Option key={p.id} label={p.name} value={p.id} />
            ))}
          </Select>
            <EmailNote />
        </Content>
      </Container>
    )
  }
}

export default connect(
  state => ({
    companyId: state.installs.form.companyId,
    companies: state.dataFetch.companies
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallCompanyScreenComponent)

const EmailNote = () =>
  <View
    style={{
      width: '100%',
      marginTop: 40,
      marginBottom: 40,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}
  >
    <Text>{I18n.t('installs.needCompany')} {I18n.t('installs.requestCompany')} </Text>
    <View
      style={{
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',
      }}
    >
      <Link
        title={I18n.t('common.supportEmail')}
        url={
          'mailto:' +
            I18n.t('common.supportEmail') +
            '?&subject=Globe Tracker Installation App Company Access Request'
        }
      />
    </View>
  </View>
