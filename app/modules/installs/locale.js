import I18n from '../../locale/setup'

I18n.translations.en.installsSummary = {
  id: 'ID',
  type: 'Type',
  location: 'Location',
  noAssetId: 'No asset ID filled in',
  noAssetType: 'No asset type filled in',
  noCompanyId: 'No company ID filled in',
  noSensors: 'No sensors added',
  noCommUnitId: 'No comm unit ID filled in',
  commUnitPlacement: 'Placement',
  company: 'Company',
  asset: 'Asset',
  commUnit: 'Comm Unit',
  sensors: 'Sensors',
  note: 'Data will be uploaded after you submit this installation. If there is no internet connection, you can still send data later from a List of installations screen.'
}
I18n.translations.en.installs = {
  errorTestAppNotInstalled: 'TestApp not installed on the device',
  errorUnknown: 'Unknown error',
  navTitle: 'Installations',
  navTitleCompany: 'Company',
  navTitleAsset: 'Asset',
  navTitleAssetDetails: 'Asset Details',
  navTitleComm: 'Comm Unit',
  navTitleSensor: 'Sensors',
  navTitleReview: 'Summary',

  newInstall: 'New Installation',
  existingInstalls: 'List of Installations',

  controllerManufacturer: 'Controller Manufacturer',
  controllerSelect: 'Select',

  containerType: 'Container Type',
  containerTypeSelect: 'Select',
  containerTypeText: 'Container Type',

  commPlacement: 'Comm Unit Placement',
  commPlacementSelect: 'Select',
  commPlacementText: 'Comm Unit Placement',

  containerSize: 'Container Size',
  containerSizeSelect: 'Select',
  containerSizeText: 'Container Size',

  certification: 'Certification',
  certificationPlaceholder: 'Certification',

  notes: 'Notes',
  notesPlaceholder: 'Notes',

  needCompany: 'Company not on the list?',
requestCompany: 'Contact:',
  company: 'Company',
  selectCompany: 'Select',

  project: 'Project',
  selectProject: 'Select',

  assetId: 'Asset ID',
  enterAssetId: 'Enter Asset ID',

  assetType: 'Asset Type',
  selectAssetType: 'Select',
  textAssetType: 'Enter Asset Type',

  viewGuidelines: 'View Guidelines',
  guidelines: 'Guidelines',

  commId: 'Comm Unit ID',
  enterCommId: 'Type or Scan',

  sensorId: 'Sensor ID',
  enterSensorId: 'Type or Scan',

  sensorsAssigned: 'Sensors Assigned',

  sensorType: 'Sensor Type',
  sensorTypeTemperature: 'Temperature',
  sensorTypeHumidity: 'Humidity',
  sensorTypeShock: 'Shock',
  sensorTypeMotion: 'Motion',
  sensorTypeLight: 'Light',
  sensorTypeDoor: 'Door',

  sensorLocation: 'Sensor Location',
  selectSensorLocation: 'Select',
  sensorLocationInternalFront: 'Internal Front',
  sensorLocationInternalMiddle: 'Internal Middle',
  sensorLocationInternalBack: 'Internal Back',
  sensorLocationAmbientEngSide: 'Ambient End Side',
  sensorLocationAmbientDoorSide: 'Ambient Door Side',
  sensorLocationOther: 'Other',

  sensorSaveAdd: 'Add Another',
  sensorSaveComplete: 'Complete Install',

  defaultProject: 'Default project',
  launchTest: 'Launch Test',
  everythingUploaded: 'Everything uploaded',
  uploadInProgress: 'Upload in progress',
  uploadAll: 'Upload All',
  noInstallsAvailable: 'No installs available',
  back: 'Back',
  next: 'Continue',
  summary: 'Summary',
  submit: 'Submit',

  warningTitle: 'Warning!',
  warningMessage: 'If you leave before submitting installation, your changes will be lost.',
  cancelButtonTitle: 'Cancel',
  okButtonTitle: 'OK'
}

I18n.translations.en.scan = {
  asset: 'Scan a bar code',
  commUnit: 'Scan a bar code',
  sensor: 'Scan a bar code'
}

I18n.translations.en.camera = {
  capture: 'CAPTURE',
  sensorPicture: 'Take a picture',
  commUnitPicture: 'Take a picture',
  installationPicture: 'Take a picture',
  overlayTitle: 'Saving...'
}

I18n.translations.en.title = {
  commUnit: 'Comm Unit',
  sensors: 'Sensors',
  scanSensor: 'Add sensor',
  sensorPictures: 'Sensor Pictures',
  pictureName: 'Picture name',
  addPicture: 'Add picture'
}
I18n.translations.en.hint = {
  notISOCompliant: 'Hint: Not ISO 6346 compliant'
}

I18n.translations.es.installs = {
  errorTestAppNotInstalled: 'TestApp no está instalado en el dispositivo',
  errorUnknown: 'Error desconocido',
  navTitle: 'Instalación',
  navTitleCompany: 'Empresa',
  navTitleAsset: 'Activos',
  navTitleAssetDetails: 'Detalle del activo',
  navTitleComm: 'Unidad de comunicación',
  navTitleSensor: 'Sensors',
  navTitleReview: 'Resumen',
  newInstall: 'Nueva instalación',
  existingInstalls: 'Instalación existente',
  controllerManufacturer: 'Fabricante del Controlador',
  controllerSelect: 'Seleccionar',
  containerType: 'Tip de Envase',
  containerTypeSelect: 'Seleccionar',
  containerTypeText: 'Tip de Envase',
  commPlacement: 'Colocación de unidades de comunicación',
  commPlacementSelect: 'Seleccionar',
  commPlacementText: 'Colocación de unidades de comunicación',
  containerSize: 'Tamaño del contenedor',
  containerSizeSelect: 'Seleccionar',
  containerSizeText: 'Tamaño del contenedor',
  certification: 'Proceso de Certificado',
  certificationPlaceholder: 'Proceso de Certificado',
  notes: 'Notas',
  notesPlaceholder: 'Notas',
  needCompany: '¿La empresa no está en la lista?',
  requestCompany: 'Solicitar acceso',
  company: 'Empresa',
  selectCompany: 'Seleccionar empresa',
  project: 'Proyecto',
  selectProject: 'Seleccionar proyecto',
  assetId: 'ID de Activos',
  enterAssetId: 'Escriba o escanee el ID de activos',
  assetType: 'Tipo de Propietado',
  selectAssetType: 'Seleccione',
  textAssetType: 'Escribo Tipo de Propietado',
  viewGuidelines: 'Procedimientos',
  guidelines: 'Procedimientos',
  commId: 'ID de Unidad de Comunicación',
  enterCommId: 'Escriba o escanee la identificación de la unidad de comunicación',
  sensorId: 'ID del Sensor',
  enterSensorId: 'Escriba o Escanee',
  sensorsAssigned: 'Sensores asignados',
  sensorType: 'Tipo de Sensor',
  sensorTypeTemperature: 'Temperatura',
  sensorTypeHumidity: 'Humedad',
  sensorTypeShock: 'Choque',
  sensorTypeMotion: 'Movimiento',
  sensorTypeLight: 'Luz',
  sensorTypeDoor: 'Puerta',
  sensorLocation: 'Ubicación del sensor',
  selectSensorLocation: 'Seleccione',
  sensorLocationInternalFront: 'Frente Interno',
  sensorLocationInternalMiddle: 'Medio Interno',
  sensorLocationInternalBack: 'Parte trasera interna',
  sensorLocationAmbientEngSide: 'Ambiente de ultimo lado',
  sensorLocationAmbientDoorSide: 'Ambiente puerta lado',
  sensorLocationOther: 'Otro',
  sensorSaveAdd: 'Agrega Otro',
  sensorSaveComplete: 'Instalación Completa',
  defaultProject: 'Proyecto predeterminado',
  launchTest: 'Prueba de lanzamiento',
  everythingUploaded: 'Todo subido',
  uploadInProgress: 'Carga en progreso',
  uploadAll: 'Subir todo',
  noInstallsAvailable: 'No hay instalaciones disponibles',
  next: 'Continuar',
  summary: 'Resumen',
  submit: 'Enviar',
  warningTitle: '¡Advertencia!',
  warningMessage: 'Si se va antes de enviar la instalación, los cambios se perderán.',
  cancelButtonTitle: 'Cancelar',
  okButtonTitle: 'OK'
}

I18n.translations.es.installsSummary = {
  id: 'ID',
  type: 'Tipo',
  location: 'Ubicación',
  noAssetId: 'No se ha proporcionado ningún ID de activo',
  noAssetType: 'Ningún Tipo de Propietado suministrado',
  noCompanyId: 'No se ha proporcionado ningún ID de empresa',
  noSensors: 'No se han añadido sensores',
  noCommUnitId: 'No se suministró ningún ID de unidad de comunicación',
  commUnitPlacement: 'Colocación',
  company: 'Empresa',
  asset: 'Activo',
  commUnit: 'Unidad de comunicación',
  sensors: 'Sensores',
  note: 'Los datos se cargarán después de enviar esta instalación. Si no hay conexión a Internet, puede enviar datos posteriormente desde la pantalla Lista de instalaciones.'
}

I18n.translations.es.scan = {
  asset: 'Escanear un código de barras',
  commUnit: 'Escanear un código de barras',
  sensor: 'Escanear un código de barras'
}

I18n.translations.es.camera = {
  capture: 'CAPTURAR',
  sensorPicture: 'Toma una foto',
  commUnitPicture: 'Toma una foto',
  installationPicture: 'Toma una foto'
}
I18n.translations.es.title = {
  commUnit: 'Unidad de comunicación:',
  sensors: 'Sensores:',
  scanSensor: 'Añadir sensor',
  sensorPictures: 'Sensor imágenes',
  pictureName: 'Nombre de imagen',
  addPicture: 'Añadir imagen'
}
I18n.translations.es.hint = {
  notISOCompliant: 'No es compatible con ISO 6346'
}

export default I18n
