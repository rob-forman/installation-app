import { AsyncStorage } from 'react-native'
import { call, all, takeEvery, takeLatest, select, put } from 'redux-saga/effects'
import RNFS from 'react-native-fs'
import { types } from './actions'
import { extractImageUrisFromInstallCommands, isImageUriStoredInInstallCommands } from './utils'
import { types as authTypes } from '../auth/actions'
import { postInstall } from '../auth/api'
import { imagesDirectory } from '../common/constants'
import { tryRefreshAccessToken } from '../auth/sagas'

const finishedInstallsRE = /storage\.finishedInstalls\.(.+)$/m
const StorageKeys = {
  finishedInstalls: 'storage.finishedInstalls'
}

function* storeSingleUserInstalls(username, installs) {
  yield call(
    AsyncStorage.setItem,
    `${StorageKeys.finishedInstalls}.${username}`,
    JSON.stringify(installs)
  )
}

function* getSingleUserInstalls(key) {
  const res = finishedInstallsRE.exec(key)
  const username = res && res[1]
  if (username) {
    const record = yield call(AsyncStorage.getItem, key)
    return {
      username,
      install: JSON.parse(record)
    }
  }
  return null
}

function* getStoredInstalls() {
  const storageKeys = yield call(AsyncStorage.getAllKeys)
  const installs = yield all(
    storageKeys
      .filter(item => finishedInstallsRE.exec(item))
      .map(key => call(getSingleUserInstalls, key))
  )
  return installs.reduce((memo, value) => {
    if (value) {
      memo[value.username] = value.install
    }
    return memo
  }, {})
}

function* uploadSingleInstall(install, username, token) {
  if(install.isUploaded) {
    return {
      username,
      install
    }
  }
  try {
    yield call(postInstall, token, install.install)
    return {
      username,
      install: {
        ...install,
        isUploaded: true
      }
    }
  } catch(err) {
    console.log(err)
    return {
      username,
      install
    }
  }
}

// Takes all installs from all users merges them
// and sends for upload, takes response transforms it back to localStorage format,
// stores it to the appState
function* uploadInstalls(currentStore) {
  const token = yield select(state => state.auth.authenticatedUser.accessToken)
  const updatedInstalls = yield all(Object.keys(currentStore).reduce((memo, username) => {
    return [
      ...memo,
      ...currentStore[username]
        .map(install => call(uploadSingleInstall, install, username, token))
    ]
  }, []))
  const newStore = updatedInstalls.reduce((memo, item) => {
    if(memo[item.username]) {
      memo[item.username].push(item.install)
    } else {
      memo[item.username] = [item.install]
    }
    return memo
  }, {})
  return newStore
}

function* pruneImageDirectory(allInstalls) {
  const readImageDirectory = () => (
    RNFS.readDir(imagesDirectory).catch(error => {
      console.log(error)
      return []
    })
  )

  const allImageUris = new Set()
  for (const username in allInstalls) {
    if (allInstalls.hasOwnProperty(username)) {
      for (const uri of extractImageUrisFromInstallCommands(allInstalls[username])) {
        allImageUris.add(uri)
      }
    }
  }

  const content = yield call(readImageDirectory)
  for (const file of content) {
    const imageUri = `file://${file.path}`
    if (!allImageUris.has(imageUri)) {
      console.log(`Deleting ${imageUri}`)
      RNFS.unlink(file.path).catch(error => {
        console.warn(error)
      })
    }
  }
}

// sets current user installs to appstate
function* setInstalls(store) {
  const currentUsername = yield select(state => state.auth.authenticatedUser.username)
  yield put({
    type: types.SET_INSTALLS_FROM_LOCAL_STORAGE,
    value: store[currentUsername]
  })
}

// When new install finished takes all current user installs
// merges them with all installs from local storage
// and uploads them
function* uploadAll() {
  yield put({ type: types.UPLOADING_IN_PROGRESS, value: true })

  const refreshToken = yield select(state => state.auth.authenticatedUser.refreshToken)
  try {
    yield call(tryRefreshAccessToken, refreshToken)
  } catch (e) {
    console.warn('Refresh token refresh failed')
    yield put({ type: types.UPLOADING_IN_PROGRESS, value: false })
    return
  }
  const currentInstalls = yield select(state => state.installs.installCommands)
  const currentUsername = yield select(state => state.auth.authenticatedUser.username)
  const currentStore = yield call(getStoredInstalls)
  currentStore[currentUsername] = currentInstalls
  const updatedInstalls = yield call(uploadInstalls, currentStore)
  yield call(setInstalls, updatedInstalls)
  yield all(
    Object.keys(updatedInstalls)
      .map(username =>
        call(storeSingleUserInstalls, username, updatedInstalls[username])))
  yield put({ type: types.UPLOADING_IN_PROGRESS, value: false })
  yield pruneImageDirectory(updatedInstalls)
}

// Loads data from localstorage
function* onUserLoaded() {
  const currentUsername = yield select(state => state.auth.authenticatedUser.username)
  const usersInstalls = yield call(AsyncStorage.getItem, `${StorageKeys.finishedInstalls}.${currentUsername}`)
  yield put({
    type: types.SET_INSTALLS_FROM_LOCAL_STORAGE,
    value: JSON.parse(usersInstalls || '[]')
  })
}

function* onUnlinkImages({ value: imageUris }) {
  const commands = yield select(state => (state.installs.installCommands))
  for (const imageUri of imageUris) {
    if (!isImageUriStoredInInstallCommands(commands, imageUri)) {
      RNFS.unlink(imageUri).catch(error => {
        console.warn(error)
      })
    }
  }
}

export default function* loginSaga() {
  yield all([
    takeLatest([types.FORM_COMPLETE_INSTALL, types.FORCE_UPLOAD_ALL], uploadAll),
    takeEvery(authTypes.LOGIN_SUCCESS, onUserLoaded),
    takeEvery(types.UNLINK_IMAGES, onUnlinkImages)
  ])
}
