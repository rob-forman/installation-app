import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, ButtonGuide } from '../common/ui/Buttons'
import {
  Container, Content, List, ListItem, Body,
  Label, Separator, Text, Left, Right
} from 'native-base'
import { IconRemove, SensorIcon } from '../common/ui/Icons'
import { Row } from '../common/ui'
import { SPACING, COLOR_LIGHT_GREY, COLOR_DARK_GREY, COLOR_WHITE } from '../common/ui/constants'
import actions from './actions'
import styles from './styles'
import I18n from './locale'

import {getSensorName} from './constants/sensorLocations'

class InstallSensorsScreenComponent extends Component {

  keyExtractor = (sensor) => (sensor.id)

  onPressDeleteButton = (sensor) => {
    this.props.actions.deleteSensor(sensor)
  }

  navigateToSensor = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'InstallSensor'
    })
    this.props.navigateTo(navigateAction)
  }

  navigateToGuidelines = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'GuidelineManufacturers',
      params: 'sensors'
    })
    this.props.navigateTo(navigateAction)
  }

  render() {
    return (
      <Container style={styles.page}>
        <Row center padding={SPACING * 2} backgroundColor={COLOR_WHITE}>
          <ButtonGuide icon="book" block onPress={this.navigateToGuidelines}>{I18n.t('installs.viewGuidelines')}</ButtonGuide>
        </Row>
        <Content
          style={styles.contentNoPadding}
          keyboardDismissMode="on-drag"
        >
          <List>
            <Separator style={styles.separator} bordered>
              <Label style={styles.label}>{I18n.t('title.commUnit')}</Label>
            </Separator>
            <ListItem last>
              <Text>{this.props.commId.value}</Text>
            </ListItem>
            <Separator style={styles.separator} bordered>
              <Label style={styles.label}>{I18n.t('title.sensors')}</Label>
            </Separator>
            {this.props.sensors.map(s =>
              <ListItem key={s.id}>
                <Body>
                  <Text>{s.id}</Text>
                  <Text note>{getSensorName(s.location)}</Text>
                </Body>
                <Left>
                  { Object.keys(s.type).map(type =>
                    <SensorIcon key={type} type={type} size={24} color={COLOR_DARK_GREY} />
                  )}
                </Left>
                <Right>
                  <IconRemove onPress={() => this.onPressDeleteButton(s)} />
                </Right>
              </ListItem>
            )}
            <ListItem last style={styles.center}>
            <Row center>
              <Button icon="disc" block onPress={this.navigateToSensor}>{I18n.t('title.scanSensor')}</Button>
            </Row>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

export default connect(
  state => ({
    commId: state.installs.form.commId,
    sensors: state.installs.current.sensors
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallSensorsScreenComponent)
