import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import actions from './actions'
import BarcodeScanner from './BarcodeScanner'
import I18n from './locale'

const ScanValueType = {
  Asset: 'Asset',
  CommUnit: 'CommUnit',
  Sensor: 'Sensor'
}

class ScanBarcodeScreenComponent extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: (() => {
      switch (navigation.state.params) {
      case ScanValueType.Asset: return I18n.t('scan.asset')
      case ScanValueType.CommUnit: return I18n.t('scan.commUnit')
      case ScanValueType.Sensor: return I18n.t('scan.sensor')
      }
    })()
  })

  onBarcodeRead = (event) => {
    if (!this.hasScannedCode) {
      this.hasScannedCode = true

      const goBack = this.props.navigation.goBack.bind(this.props.navigation)
      switch (this.props.navigation.state.params) {
      case ScanValueType.Asset:
        this.props.actions.formAssetId(event.data, goBack)
        break
      case ScanValueType.CommUnit:
        this.props.actions.formCommId(event.data, goBack)
        break
      case ScanValueType.Sensor:
        this.props.actions.formSensorId(event.data, goBack)
        break
      }
    }
  }

  componentDidMount() {
    this.hasScannedCode = false
  }

  render() {
    return <BarcodeScanner onRead={this.onBarcodeRead} />
  }
}

const ScanBarcodeScreen = connect(
  () => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(ScanBarcodeScreenComponent)

export default ScanBarcodeScreen
