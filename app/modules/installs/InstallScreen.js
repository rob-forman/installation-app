import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from './actions'
import I18n from './locale'
import { NavigationActions } from 'react-navigation'
import { Alert, BackHandler, ScrollView, View } from 'react-native'
import { createSelector } from 'reselect'
import InstallCompanyScreen from '../installs/InstallCompanyScreen'
import InstallAssetScreen from '../installs/InstallAssetScreen'
import InstallAssetMoreScreen from '../installs/InstallAssetMoreScreen'
import InstallCommScreen from '../installs/InstallCommScreen'
import InstallSensorsScreen from '../installs/InstallSensorsScreen'
import InstallSummaryScreen from '../installs/InstallSummaryScreen'
import { Breadcrumbs } from '../common/ui'
import { ButtonNext, ButtonPrev, ButtonDone } from '../common/ui/Buttons'
import styles from './styles'

import dimensions from 'Dimensions'

const showWarning = (navigation) => {

  const buttons = [
    { text: I18n.t('installs.cancelButtonTitle'), style: 'cancel'},
    { text: I18n.t('installs.okButtonTitle'), onPress: () => { navigation.goBack() }},
  ]

  const title = I18n.t('installs.warningTitle')
  const message = I18n.t('installs.warningMessage')

  Alert.alert(title, message, buttons, { cancelable: false } )
}

class InstallScreenComponent extends Component {

  static navigationOptions = ({ navigation }) => {
    const { state } = navigation
    const { currentPage } = state.params || {}

    const title = (() => {
      switch (currentPage || 0) {
      case 0: return I18n.t('installs.navTitleCompany')
      case 1: return I18n.t('installs.navTitleAsset')
      case 2: return I18n.t('installs.navTitleAssetDetails')
      case 3: return I18n.t('installs.navTitleComm')
      case 4: return I18n.t('installs.navTitleSensor')
      case 5: return I18n.t('installs.navTitleReview')
      case 6: return '!Overview'
      }
    })()

    return {
      title
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      currentPage: 0
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBack)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack)
  }

  handleBack = () => {
    showWarning(this.props.navigation)
    return true
  }

  scrollTo(page) {
    const { width } = dimensions.get('window')
    this.scrollView.scrollTo({x: page * width, y: 0, animated: true })
  }

  onBack = () => {
    this.scrollTo(Math.max(this.state.currentPage - 1, 0))
  }

  onNext = () => {
    this.scrollTo(Math.min(this.state.currentPage + 1, this.props.pages.length - 1))
  }

  onDone = () => {
    this.props.actions.completeInstall()
    const action = NavigationActions.back({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'InstallList' })
      ]
    })
    this.props.navigation.dispatch(action)
  }

  onScroll = (event) => {
    const { width } = dimensions.get('window')
    const currentPage = Math.round(event.nativeEvent.contentOffset.x / width)
    if (this.state.currentPage != currentPage) {
      this.setState({ currentPage })
      this.props.navigation.setParams({ currentPage })
      this.props.actions.visitPage(currentPage)
    }
  }

  get isFirstPage() {
    return this.state.currentPage == 0
  }

  get isLastPage() {
    return this.state.currentPage == this.props.pages.length - 1
  }

  get nextButtonTitle() {
    return this.props.pages[this.state.currentPage].nextButtonTitle
      || I18n.t('installs.next')
  }
    
    get backButtonTitle() {
        return this.props.pages[this.state.currentPage].backButtonTitle
        || I18n.t('installs.back')
    }
    
  setScrollView = (scrollView) => {
    this.scrollView = scrollView
  }

  navigateTo = (action) => {
    this.props.navigation.dispatch(action)
  }

  render() {
    const isEveryPageCompleted = this.props.pages.every(page => page.isCompleted)
    return (
      <View style={{ height: '100%' }}>
        <Breadcrumbs
          active={this.state.currentPage}
          items={this.props.pages}
          values={this.props.values}
        />
        <ScrollView onScroll={this.onScroll}
          ref={this.setScrollView}
          pagingEnabled
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={2}
          bounces={false}
          keyboardDismissMode="on-drag"
        >
          <InstallCompanyScreen navigateTo={this.navigateTo} />
          <InstallAssetScreen navigateTo={this.navigateTo} />
          <InstallAssetMoreScreen navigateTo={this.navigateTo} />
          <InstallCommScreen navigateTo={this.navigateTo} />
          <InstallSensorsScreen navigateTo={this.navigateTo} />
          <InstallSummaryScreen navigateTo={this.navigateTo} />
        </ScrollView>

        <View style={styles.fixedBottom}>
          { !this.isFirstPage &&
            <ButtonPrev onPress={this.onBack}
            style={styles.pushLeft}
            >{this.backButtonTitle}</ButtonPrev>
          }
          { this.isLastPage ?
            <ButtonDone
              onPress={this.onDone}
              disabled={!isEveryPageCompleted}
              style={styles.pushRight}
            >{this.nextButtonTitle}</ButtonDone>
            :
            <ButtonNext
              onPress={this.onNext}
              style={styles.pushRight}
            >{this.nextButtonTitle}</ButtonNext>
          }
        </View>
      </View>
    )
  }
}

const getFormValues = createSelector(
  state => state.installs.form.companyId,
  state => state.installs.form.assetId,
  state => state.installs.form.commId,
  state => state.installs.current.sensors,
  state => state.dataFetch.companies,
  (companyId, assetId, commId, sensors, companies) => ({
    company: (companies.find(company => company.id == companyId.value) || {}).name,
    assetId: assetId.value,
    commId: commId.value,
    sensors: sensors.length,
  })
)

const isCompanyCompleted = createSelector(
  state => state.installs.form.companyId,
  companyId => !!(companyId.value)
)

const isAssetCompleted = createSelector(
  state => state.installs.form.assetId,
  state => state.installs.form.assetType,
  (assetId, assetType) => !!(assetId.value) && !!(assetType.value)
)

const isAssetMoreCompleted = createSelector(
  state => state.installs.current.customFields.manufacturer,
  state => state.installs.current.customFields.containerType,
  state => state.installs.current.customFields.containerSize,
  (isManufacturer, isContainerType, isContainerSize) =>
    !!isManufacturer && !!isContainerType && !!isContainerSize
)

const isCommUnitCompleted = createSelector(
  state => state.installs.form.commId,
  commId => !!(commId.value)
)

const isSensorsCompleted = createSelector(
  state => !!(state.installs.form.visitedPages
    && state.installs.form.visitedPages[4]),
  isCompleted => isCompleted
)

const getPages = createSelector(
  isCompanyCompleted,
  isAssetCompleted,
  isAssetMoreCompleted,
  isCommUnitCompleted,
  isSensorsCompleted,
  () => false,
  (...isCompleted) => {
    const pages = [
      { name: 'Company', id: 'C' },
      { name: 'Asset', id: 'A' },
      { name: 'Asset Details', id: 'AD' },
      { name: 'Comm Unit', id: 'CU' },
      { name: 'Sensors', id: 'S', nextButtonTitle: I18n.t('installs.summary') },
      { name: 'Summary', id: 'Sum', nextButtonTitle: I18n.t('installs.submit'), isCompleted: true }
    ]
    // last page - overview should be completed if all previous steps are
    for (let i = 0; i < pages.length - 1; i++) {
      pages[i].isCompleted = isCompleted[i]
      pages[pages.length - 1].isCompleted &= isCompleted[i]
    }

    return pages
  }
)

export default connect(
  state => ({
    values: getFormValues(state),
    pages: getPages(state)
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallScreenComponent)
