import React, { Component } from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import Camera from 'react-native-camera'
import ImageResizer from 'react-native-image-resizer'
import RNFS from 'react-native-fs'

import I18n from './locale'
import { Button } from '../common/ui/Buttons'
import { imagesDirectory } from '../common/constants'

class CameraWrapper extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isPictureTaken: false
    }
  }

  setCamera = (camera) => {
    this.camera = camera
  }

  renderOverlay() {
    console.log('Render overlay')
    return (
      <View style={styles.overlay}>
        <ActivityIndicator size="large" color="white" animating={true} />
        <Text style={styles.overlayTitle}>{I18n.t('camera.overlayTitle')}</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Camera
          ref={this.setCamera}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}
        >
          <Button
            icon="camera"
            disabled={this.state.isPictureTaken}
            size={36}
            onPress={this.takePicture}
            style={button}
          >{I18n.t('camera.capture')}</Button>
        </Camera>
        {this.state.isPictureTaken ? this.renderOverlay() : null }
      </View>
    )
  }

  processImage({ path }) {
    return RNFS.mkdir(imagesDirectory).then(() =>
      ImageResizer.createResizedImage(
        path,
        1600,
        1600,
        'JPEG',
        80,
        0,
        imagesDirectory)
    ).then(resp =>
      RNFS.unlink(path).then(() => (resp))
    )
  }

  takePicture = () => {
    this.setState({
      isPictureTaken: true
    })

    const options = {
      target: Camera.constants.CaptureTarget.disk
    }

    this.camera.capture(options)
      .then(this.processImage)
      .then(this.props.onPictureTaken)
      .catch(this.props.onError)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    backgroundColor: 'white',
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  overlayTitle: {
    color: 'black',
    fontSize: 24
  }
})

const button = {
  flex: 0,
  marginBottom: 40,
  marginLeft: 'auto',
  marginRight: 'auto'
}

export default CameraWrapper
