import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { createSelector } from 'reselect'
import { View } from 'react-native'
import { Container, Content, List, ListItem, Left, Body, Right, Text } from 'native-base'

import I18n from './locale'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import actions from './actions'
import { COLOR_WHITE, COLOR_GREY, BASELINE, COLOR_BLUE } from '../common/ui/constants'
import { Button } from '../common/ui/Buttons'

const Empty = () =>
  <View style={styles.empty}>
    <Icon name="clipboard-outline" size={60} color={COLOR_GREY} />
    <Text style={styles.emptyText}>{I18n.t('installs.noInstallsAvailable')}</Text>
  </View>

class InstallListScreenComponent extends Component {
  static navigationOptions = {
    title: I18n.t('installs.existingInstalls')
  }

  navigateToInstall = () => this.props.navigation.navigate('Install')

  onItemPress = (installCommand) => this.props.actions.loadInstall(
    {
      form: installCommand.form,
      current: installCommand.install,
      id: installCommand.id
    },
    this.navigateToInstall
  )

  renderListItem = installCommand =>
    <ListItem icon button key={installCommand.install.id} onPress={() => this.onItemPress(installCommand)} >
      <Left>
        <Icon
          name={installCommand.isUploaded ? 'cloud-check' : 'cloud-outline'}
          color={COLOR_BLUE}
          size={20}
        />
      </Left>
      <Body>
        <Text>
          {installCommand.install.asset.id}
        </Text>
        <Text note>
          {new Date(installCommand.date).toLocaleString()}
        </Text>
      </Body>
      <Right>
        <Icon name="chevron-right" size={28} />
      </Right>
    </ListItem>

  mostRecentInstallCommandFirst = (command1, command2) =>
    command2.date - command1.date

  render() {
    const buttonTitle = this.props.allInstallsUploaded
      ? I18n.t('installs.everythingUploaded')
      : this.props.isUpoading
        ? I18n.t('installs.uploadInProgress')
        : I18n.t('installs.uploadAll')
    return (
      <Container>
        <Content style={styles.mainView}>
          { this.props.installCommands.length == 0 ?
            <Empty/> :
            <List dataArray={
              this.props.installCommands.sort(this.mostRecentInstallCommandFirst)
              } renderRow={this.renderListItem}
            />
          }
        </Content>
        <Button
          full
          icon="cloud-upload"
          onPress={this.props.actions.forceUploadAll}
          disabled={this.props.allInstallsUploaded || this.props.isUploading}>
          {buttonTitle}
        </Button>
      </Container>
    )
  }
}

const allInstallsUplaodedSelector = createSelector(
  state => state.installs.installCommands
    .map(item => item.isUploaded),
    installsUploaded => installsUploaded
      .every(item => item)
)

export default connect(
  state => ({
    isUploading: state.installs.isUploading,
    installCommands: state.installs.installCommands,
    allInstallsUploaded: allInstallsUplaodedSelector(state),
    accessToken: state.auth.authenticatedUser.accessToken
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallListScreenComponent)

const styles = {
  mainView: {
    backgroundColor: COLOR_WHITE,
  },
  empty: {
    marginTop: BASELINE,
    alignItems: 'center',
  },
  emptyText: {
    color: COLOR_GREY,
    fontSize: 20,
  }
}
