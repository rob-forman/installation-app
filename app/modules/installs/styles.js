import dimensions from 'Dimensions'
import * as UI from '../common/ui/constants'
import { Platform, StyleSheet } from 'react-native'
import { BASELINE } from '../common/ui/constants'
const { width } = dimensions.get('window')
const footerHeight = Platform.OS == 'ios' ? 196 : 192

export default {
  page: {
    flex: 1,
    backgroundColor: '#444444',
    padding: 0,
    width: width,
  },
  content: {
    flex: 1,
    backgroundColor: UI.COLOR_WHITE,
    padding: UI.BASELINE / 2,
    marginBottom: footerHeight,
  },
  contentNoPadding: {
    flex: 1,
    backgroundColor: UI.COLOR_WHITE,
    marginBottom: footerHeight,
  },
  row: {
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'center',
  },
  picker: {
    // width: '100%',
    borderColor: 'black',
    borderWidth: 2,
  },
  pushRight: {
    marginLeft: 'auto',
  },
  pushLeft: {
    marginRight: 'auto',
  },
  marginLeft: {
    marginLeft: UI.SPACING * 2,
  },
  marginRight: {
    marginRight: UI.SPACING * 2,
  },
  fixedBottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: '#d1d1d1',
    padding: UI.BASELINE / 2,
    backgroundColor: UI.COLOR_WHITE,
  },
  label: {
    fontSize: 13,
    color: '#333333',
  },
  marginTop: {
    marginTop: 40,
  },
  input: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#dedede',
    paddingLeft: 17,
},
  text: {
    fontWeight: 'normal',
},
  separator: {
    backgroundColor: UI.COLOR_LIGHT_GREY_BLUE,
}
}
