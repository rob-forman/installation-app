import { isBlank } from '../../tools/Validation'
import I18n from './locale'

import { isAssetIdValid } from '../common/utils'

const ns = 'INSTALLS'
export const types = {
  RESET: ns + '/RESET',
  RESET_FORM: ns + '/RESET_FORM',
  RESET_CURRENT: ns + '/RESET_CURRENT',

  FORM_COMPANY_ID_PENDING: ns + '/FORM_COMPANY_ID_PENDING',
  FORM_COMPANY_ID_SUCCESS: ns + '/FORM_COMPANY_ID_SUCCESS',
  FORM_COMPANY_ID_FAILURE: ns + '/FORM_COMPANY_ID_FAILURE',

  SET_CURRENT_COMPANY: ns + '/SET_CURRENT_COMPANY',

  FORM_ASSET_ID_PENDING: ns + '/FORM_ASSET_ID_PENDING',
  FORM_ASSET_ID_SUCCESS: ns + '/FORM_ASSET_ID_SUCCESS',
  FORM_ASSET_ID_FAILURE: ns + '/FORM_ASSET_ID_FAILURE',
  FORM_ASSET_TYPE_FAILURE: ns + '/FORM_ASSET_TYPE_FAILURE',
  FORM_ASSET_TYPE_SUCCESS: ns + '/FORM_ASSET_TYPE_SUCCESS',

  SET_CURRENT_ASSET: ns + '/SET_CURRENT_ASSET',
  SET_ASSET_TYPE: ns + '/SET_ASSET_TYPE',

  FORM_COMM_PLACEMENT: ns + '/FORM_COMM_PLACEMENT',

  FORM_COMM_ID_PENDING: ns + '/FORM_COMM_ID_PENDING',
  FORM_COMM_ID_SUCCESS: ns + '/FORM_COMM_ID_SUCCESS',
  FORM_COMM_ID_FAILURE: ns + '/FORM_COMM_ID_FAILURE',

  SET_CURRENT_COMM: ns + '/SET_CURRENT_COMM',

  FORM_SENSOR_ID_PENDING: ns + '/FORM_SENSOR_ID_PENDING',
  FORM_SENSOR_ID_SUCCESS: ns + '/FORM_SENSOR_ID_SUCCESS',
  FORM_SENSOR_ID_FAILURE: ns + '/FORM_SENSOR_ID_FAILURE',

  FORM_SENSOR_TYPE_CHECKED: ns + '/FORM_SENSOR_TYPE_CHECKED',
  FORM_SENSOR_TYPE_UNCHECKED: ns + '/FORM_SENSOR_TYPE_UNCHECKED',

  FORM_CUSTOM_FIELD_CHANGED: ns + '/FORM_CUSTOM_FIELD_CHANGED',

  FORM_SENSOR_LOCATION_SUCCESS: ns + '/FORM_SENSOR_LOCATION_SUCCESS',
  FORM_SENSOR_LOCATION_FAILURE: ns + '/FORM_SENSOR_LOCATION_FAILURE',
  FORM_SENSOR_SUCCESS: ns + '/FORM_SENSOR_SUCCESS',
  DELETE_SENSOR: ns + '/DELETE_SENSOR',
  RESET_SENSOR: ns + '/RESET_SENSOR',
  SET_CURRENT_SENSOR: ns + '/SET_CURRENT_SENSOR',
  FORM_COMPLETE_INSTALL: ns + '/FORM_COMPLETE_INSTALL',
  FORM_SENSOR_PICTURE_SUCCESS: ns + '/FORM_SENSOR_PICTURE_SUCCESS',
  SET_CURRENT_ASSET_PICTURE_SUCCESS: ns + '/FORM_ASSET_PICTURE_SUCCESS',
  SET_CURRENT_COMM_UNIT_PICTURE_SUCCESS: ns + '/FORM_COMM_UNIT_PICTURE_SUCCESS',

  GUIDELINE_VISIBLE: ns + '/GUIDELINE_VISIBLE',
  GUIDELINE_HIDDEN: ns + '/GUIDELINE_HIDDEN',

  LOAD_INSTALL: ns + '/LOAD_INSTALL',
  SET_INSTALLS_FROM_LOCAL_STORAGE: ns + '/SET_INSTALLS_FROM_LOCAL_STORAGE',
  VISIT_PAGE: ns + '/VISIT_PAGE',
  FORCE_UPLOAD_ALL: ns + '/FORCE_UPLOAD_ALL',
  UPLOADING_IN_PROGRESS: ns + '/UPLOADING_IN_PROGRESS',
  UNLINK_IMAGES: ns + '/UNLINK_IMAGES'
}

// Validations

const assetIdHintFailure = assetId => {
  if (isBlank(assetId) || !isAssetIdValid(assetId))  {
    return {
      type: types.FORM_ASSET_ID_FAILURE,
      value: assetId,
      error: I18n.t('hint.notISOCompliant'),
    }
  }
}

const assetIdFailure = assetId => {
  if (isBlank(assetId))  {
    return {
      type: types.FORM_ASSET_ID_FAILURE,
      value: assetId,
      error: I18n.t('common.required'),
    }
  }
}

const assetTypeFailure = assetType => {
  if (isBlank(assetType))  {
    return {
      type: types.FORM_ASSET_TYPE_FAILURE,
      value: assetType,
      error: I18n.t('common.required'),
    }
  }
}

const commIdFailure = commId => {
  if (isBlank(commId)) {
    return {
      type: types.FORM_COMM_ID_FAILURE,
      value: commId,
      error: I18n.t('common.required'),
    }
  }
}

const sensorIdFailure = sensorId => {
  if (isBlank(sensorId)) {
    return {
      type: types.FORM_SENSOR_ID_FAILURE,
      value: sensorId,
      error: I18n.t('common.required'),
    }
  }
}

const sensorTypeFailure = sensorType => {
  if (isBlank(sensorType)) {
    return {
      type: types.FORM_SENSOR_TYPE_FAILURE,
      value: sensorType,
      error: I18n.t('common.required'),
    }
  }
}

const sensorLocationFailure = sensorLocation => {
  if (isBlank(sensorLocation)) {
    return {
      type: types.FORM_SENSOR_TYPE_FAILURE,
      value: sensorLocation,
      error: I18n.t('common.required'),
    }
  }
}

const reset = () => ({ type: types.RESET })
const resetForm = () => ({type: types.RESET_FORM})
const resetCurrent = () => dispatch => {
  // Current implies form reset also
  dispatch({type: types.RESET_FORM})
  dispatch({type: types.RESET_CURRENT})
}

/*  Workflow: *state.form* is a temporary structure which contains fields presented on the screen (+ error messages).
 *  Whenever we move to the next screen, data is reorganized into larger logical units in *state.current*,
 *  which refers to the current install.
 *  After completing the install current is moves into state.installs[].
 *
 *  Naming convention:
 *  Methods prefixed with *form* dispatch actions to modify the state.form
 *  Methods prefixed with *setCurrent* dispatch actions to modify the state.current
 */

// Company
const formCompany = nextCompanyId => dispatch => {
  dispatch({
    type: types.FORM_COMPANY_ID_SUCCESS,
    value: nextCompanyId,
  })
}

const setCurrentCompany = (nextCompanyId, onSuccess = () => {}) => (dispatch, getState) => {
  const { companies } = getState().dataFetch
  const company = companies.find(c => c.id == nextCompanyId)
  dispatch({
    type: types.SET_CURRENT_COMPANY,
    value: company,
  })
  onSuccess()
}

// Asset
const formAssetId = (nextAssetId, callback = () => {}) => dispatch => {
  dispatch({ type: types.FORM_ASSET_ID_PENDING })

  let failure = assetIdHintFailure(nextAssetId)
  if (failure) {
    dispatch(failure)
  } else {
    dispatch({
      type: types.FORM_ASSET_ID_SUCCESS,
      value: nextAssetId,
    })
  }
  callback()
}

const formAssetType = assetType => dispatch =>
  dispatch({type: types.SET_ASSET_TYPE, value: assetType})


const setCurrentAsset = (nextAsset, onSuccess = () => {}) => dispatch => {
  dispatch({ type: types.FORM_ASSET_ID_PENDING, value: nextAsset.id }) // reset asset ID; identical to asset_id_success
  let failure = assetIdFailure(nextAsset.id)
  if (failure) {
    dispatch(failure)
  }

  let failure2 = assetTypeFailure(nextAsset.type)
  if (failure2) {
    dispatch(failure2)
  }

  if(!failure && !failure2) {
    dispatch({
      type: types.FORM_ASSET_ID_SUCCESS,
      value: nextAsset.id,
    })
    dispatch({
      type: types.FORM_ASSET_TYPE_SUCCESS,
      value: nextAsset.id,
    })
    dispatch({
      type: types.SET_CURRENT_ASSET,
      value: nextAsset,
    })
    onSuccess()
  }
}

const formCommPlacement = (placement) => (dispatch) => dispatch({
  type: types.FORM_COMM_PLACEMENT,
  value: placement
})

// Communication unit
const formCommId = (nextCommId, callback = () => {}) => dispatch => {
  dispatch({ type: types.FORM_COMM_ID_PENDING })

  let failure = commIdFailure(nextCommId)
  if (failure) {
    dispatch(failure)
  } else {
    dispatch({
      type: types.FORM_COMM_ID_SUCCESS,
      value: nextCommId,
    })
  }
  callback()
}

// sensor
const formSensorId = (nextSensorId, callback = () => {}) => dispatch => {
  dispatch({ type: types.FORM_SENSOR_ID_PENDING })

  let failure = sensorIdFailure(nextSensorId)
  if (failure) {
    dispatch(failure)
  } else {
    dispatch({
      type: types.FORM_SENSOR_ID_SUCCESS,
      value: nextSensorId,
    })
  }
  callback()
}

const formSensorType = (checked, nextSensorTypeId, callback = () => {}) => dispatch => {
  if (checked) {
    dispatch({
      type: types.FORM_SENSOR_TYPE_CHECKED,
      value: nextSensorTypeId,
    })
  } else {
    dispatch({
      type: types.FORM_SENSOR_TYPE_UNCHECKED,
      value: nextSensorTypeId,
    })
  }
  callback()
}

const formSensorLocation = (nextSensorLocation, callback = () => {}) => dispatch => {
  dispatch({
    type: types.FORM_SENSOR_LOCATION_SUCCESS,
    value: nextSensorLocation,
  })

  callback()
}

const setCurrentCommUnitPicture = (nextPicture, callback = () => {}) => (dispatch, getState) => {
  const currentPicture = getState().installs.current.comm.picture
  if (currentPicture && currentPicture != nextPicture) {
    dispatch({
      type: types.UNLINK_IMAGES,
      value: [currentPicture]
    })
  }

  dispatch({
    type: types.SET_CURRENT_COMM_UNIT_PICTURE_SUCCESS,
    value: nextPicture
  })
  callback()
}

const setCurrentAssetPicture = (nextPicture, callback = () => {}) => (dispatch, getState) => {
  const currentPicture = getState().installs.current.asset.picture
  if (currentPicture && currentPicture != nextPicture) {
    dispatch({
      type: types.UNLINK_IMAGES,
      value: [currentPicture]
    })
  }

  dispatch({
    type: types.SET_CURRENT_ASSET_PICTURE_SUCCESS,
    value: nextPicture
  })
  callback()
}

const formSensorPicture = (nextPicture, callback = () => {}) => dispatch => {
  dispatch({
    type: types.FORM_SENSOR_PICTURE_SUCCESS,
    value: nextPicture
  })
  callback()
}

const deleteSensor = (sensor) => dispatch => {
  dispatch({
    type: types.DELETE_SENSOR,
    value: sensor,
  })
}

const setCurrentSensor = (nextSensor, onSuccess = () => {}) => dispatch => {
  //dispatch({ type: types.FORM_SENSOR_PENDING })

  let failure = sensorIdFailure(nextSensor.id)
  if (failure) {
    dispatch(failure)
  } else {
    dispatch({
      type: types.FORM_SENSOR_SUCCESS,
      value: nextSensor.id,
    })
    dispatch({
      type: types.SET_CURRENT_SENSOR,
      value: nextSensor,
    })
    onSuccess()
  }
}

const formResetSensor = () => dispatch => {
  dispatch({type: types.RESET_SENSOR})
}

const completeInstall = () => (dispatch, getState) => {
  const { form } = getState().installs
  //setCurrentCompany(form.companyId.value)
  const { companies } = getState().dataFetch
  const company = companies.find(company => company.id == form.companyId.value)
  dispatch({
    type: types.SET_CURRENT_COMPANY,
    value: company,
  })
  //setCurrentAsset({ id: form.assetId.value, type: form.assetType.value })
  const { picture } = getState().installs.current.asset
  dispatch({
    type: types.SET_CURRENT_ASSET,
    value: { id: form.assetId.value, type: form.assetType.value, picture },
  })
  //setCurrentComm(form.commId.value)
  const { picture: commPicture } = getState().installs.current.comm
  dispatch({
    type: types.SET_CURRENT_COMM,
    value: { id: form.commId.value, picture: commPicture },
  })

  dispatch({
    type: types.FORM_COMPLETE_INSTALL
  })
}

const showGuideline = () => ({ type: types.GUIDELINE_VISIBLE })
const hideGuideline = () => ({ type: types.GUIDELINE_HIDDEN })

// existing installs
const loadInstall = (install, callback = () => {}) => dispatch => {
  dispatch({type: types.LOAD_INSTALL, value: install})
  callback()
}

const visitPage = (page) => dispatch => {
  dispatch({
    type: types.VISIT_PAGE,
    value: page,
  })
}

const forceUploadAll = () => ({ type: types.FORCE_UPLOAD_ALL })

const formCustomFieldChanged = (field, value) => ({
  type: types.FORM_CUSTOM_FIELD_CHANGED,
  value: {
    field,
    value
  }
})

const actions = {
  reset,
  resetForm,
  resetCurrent,

  formCompany,

  formAssetId,
  formAssetType,
  formCustomFieldChanged,

  formCommId,
  formCommPlacement,

  formSensorId,
  formSensorType,
  formSensorLocation,
  formSensorPicture,
  setCurrentAssetPicture,
  setCurrentCommUnitPicture,
  formResetSensor,
  deleteSensor,
  setCurrentSensor,
  completeInstall,


  showGuideline,
  hideGuideline,

  loadInstall,

  visitPage,
  forceUploadAll
}

export default actions
