import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { TextInput } from 'react-native'
import { Container, Content, Label } from 'native-base'
import actions from './actions'
import styles from './styles'
import Select, { Option } from '../common/ui/Select'
import { CustomSelect, Row } from '../common/ui'
import manufacturerTypes from './constants/manufacturerTypes'
import containerTypes, { other as otherCType } from './constants/containerTypes'
import containerSizes, { other as otherSize } from './constants/containerSizes'
import I18n from './locale'
import { createSelector } from 'reselect'


class InstallAssetMoreScreenComponent extends Component {
  onCustomFieldChanged = (field) => (value) =>
    this.props.actions.formCustomFieldChanged(field, value)

  onManufacturerChanged = this.onCustomFieldChanged('manufacturer')
  onContainerTypeChanged = this.onCustomFieldChanged('containerType')
  onContainerSizeChanged = this.onCustomFieldChanged('containerSize')
  onCertificationChanged = this.onCustomFieldChanged('certification')
  onNotesChanged = this.onCustomFieldChanged('notes')

  render() {
    const {
      manufacturer,
      containerType,
      containerSize,
      certification,
      notes
    } = this.props.customFields
    return (
      <Container style={styles.page}>
        <Content style={styles.content}>
          <CustomSelect
            label={I18n.t('installs.controllerManufacturer')}
            placeholder={I18n.t('installs.controllerSelect')}
            textPlaceholder={I18n.t('installs.containerTypeText')}
            field={manufacturer}
            options={manufacturerTypes}
            onChange={this.onManufacturerChanged}
            marginBottom={0.5}
          />
          <CustomSelect
            label={I18n.t('installs.containerType')}
            placeholder={I18n.t('installs.containerTypeSelect')}
            textPlaceholder={I18n.t('installs.containerTypeText')}
            field={containerType}
            options={containerTypes}
            otherOption={otherCType}
            onChange={this.onContainerTypeChanged}
            marginBottom={0.5}
          />
          <CustomSelect
            label={I18n.t('installs.containerSize')}
            placeholder={I18n.t('installs.containerSizeSelect')}
            textPlaceholder={I18n.t('installs.containerSizeText')}
            field={containerSize}
            options={containerSizes}
            otherOption={otherSize}
            onChange={this.onContainerSizeChanged}
            marginBottom={0.5}
          />
          <Label style={styles.label}>{I18n.t('installs.certification')}</Label>
          <Row marginBottom={0.5}>
            <TextInput
              style={[localStyles.textInput, styles.input]}
              onChangeText={this.onCertificationChanged}
              value={certification}
              placeholder={I18n.t('installs.certificationPlaceholder')}
              returnKeyType="next"
            />
          </Row>
          <Label style={styles.label}>{I18n.t('installs.notes')}</Label>
          <Row marginBottom={2}>
            <TextInput
              style={[localStyles.textInput, localStyles.textarea, styles.input]}
              onChangeText={this.onNotesChanged}
              value={notes}
              multiline={true}
              placeholder={I18n.t('installs.notesPlaceholder')}
              returnKeyType="next"
            />
          </Row>
        </Content>
      </Container>
    )
  }
}

const getCustomFields = createSelector(
  state => state.installs.current.customFields,
  (customFields) => {
    let ret = {}
    for (const fieldName of Object.keys(customFields)) {
      // notes and certifications don't use our custom input fields
      ret[fieldName] = ['notes', 'certification'].includes(fieldName)
        ? customFields[fieldName]
        : {value: customFields[fieldName], error: null}
    }
    return ret
  }
)

const InstallAssetMoreScreen = connect(
  state => ({
    customFields: getCustomFields(state)
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallAssetMoreScreenComponent)

export default InstallAssetMoreScreen

const localStyles = {
  textInput: {
    height: 50,
    flex: 1,
    fontSize:17
  },
  textarea: {
    height: 120,
    textAlignVertical: 'top',
    fontSize:17,
  borderBottomWidth: 1,
  borderBottomColor: '#000000',
  }
}
