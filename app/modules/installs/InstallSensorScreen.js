import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { View, Text } from 'react-native'
import { Container, Content, Label, List, ListItem, Body, Right, Left } from 'native-base'
import { ThumbnailView } from '../common/CommonUI'
import { NavigationActions } from 'react-navigation'
import { CheckBox } from '../common/CommonUI'
import { Select, Option, TypeOrScan, Row } from '../common/ui'
import { COLOR_BLUE, COLOR_WHITE } from '../common/ui/constants'
import { IconRemove } from '../common/ui/Icons'
import { Button, ButtonDone } from '../common/ui/Buttons'
import I18n from './locale'
import actions from './actions'
import GuidelineModal from './GuidelineModal'
import styles from './styles'
import { CaptureTargetTypes } from './CaptureScreen'
import sensorTypes from './constants/sensorTypes'
import sensorLocations from './constants/sensorLocations'

class InstallSensorScreenComponent extends Component {
  constructor(props) {
    super(props)
  }

  static navigationOptions = {
    title: I18n.t('installs.navTitleSensor')
  }

  onSaveSensor = () => this.props.actions.setCurrentSensor(
    this.generateCurrentSensorObject(),
    () => {
      this.props.actions.formResetSensor()
      this.props.navigation.goBack()
    }
  )

  navigateToCapture = () => this.props.navigation.navigate('CaptureScreen', CaptureTargetTypes.sensor)

  navigateToZoomable = (image) => () => this.props.navigation.navigate('ZoomableScreen', image)

  generateCurrentSensorObject = () => ({
    id: this.props.installs.form.sensorId.value,
    type: this.props.installs.form.sensorTypes,
    location: this.props.installs.form.sensorLocation.value,
    pictures: this.props.installs.form.sensorPictures
  })

  onPressBarcodeButton = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'ScanBarcode',
      params: 'Sensor'
    })
    this.props.navigation.dispatch(navigateAction)
  }

  onSensorIdChange = text => this.props.actions.formSensorId(text)

  render() {
    return (
      <Container style={styles.page}>
        <Content style={{...styles.content, marginBottom: 0 }} keyboardDismissMode="on-drag">
          <Label style={styles.label}>{I18n.t('installs.sensorId')}</Label>
          <TypeOrScan
            field={this.props.installs.form.sensorId}
            placeholder={I18n.t('installs.sensorId')}
            onChange={this.onSensorIdChange}
            onScanPress={this.onPressBarcodeButton}
          />
          {/*
          <Select
            label={I18n.t('installs.sensorType')}
            placeholder={'Select type'}
            selectedValue={''}
            onValueChange={id => this.props.actions.formSensorType(true, id)}
            marginBottom={1}
          >
            { sensorTypes.map( s =>
              <Option key={s.id} label={s.name} value={s.id} />
            )}
          </Select>
          */}
          <Label style={styles.label}>{I18n.t('installs.sensorType')}</Label>
          { [0,1,2].map(i =>
            <View style={{ flexDirection: 'row' }} key={i}>
              {[0, 1].map(j =>
                <View style={{ width: '50%', padding: 0, margin: 2 }} key={2*i+j}>
                <CheckBox
                  label={sensorTypes[2*i + j].name}
                  size={20}
                  checked={false}
                  onChange={checked => this.props.actions.formSensorType(checked, sensorTypes[2*i + j].id)}
                  />
                </View>
              )}
            </View>
          )}
          <Select
            label={I18n.t('installs.sensorLocation')}
            placeholder={I18n.t('installs.selectSensorLocation')}
            onValueChange={value => this.props.actions.formSensorLocation(value)}
            selectedValue={this.props.installs.form.sensorLocation.value}
            marginTop={1}
          >
            {sensorLocations.map(x => (
              <Option key={x.id} label={x.name} value={x.id} />
            ))}
          </Select>
          <List style={{ marginBottom: 40 }}>
            <ListItem itemHeader style={{ paddingLeft: 0 }}>
              <Left><Label style={styles.label}>{I18n.t('title.sensorPictures')}</Label></Left>
            </ListItem>
            {this.props.installs.form.sensorPictures.map(pictureUri =>
              <ListItem key={pictureUri}>
                <ThumbnailView image={{uri: pictureUri}} onPress={this.navigateToZoomable}/>
                <Body style={{ marginLeft: 8 }}><Text>{I18n.t('title.pictureName')}</Text></Body>
                <Right>
                  <IconRemove size={24} />
                </Right>
              </ListItem>
            )}
            <ListItem last style={{ paddingRight: 0 }}>
              <Row center>
                <Button
                  block
                  icon="image"
                  onPress={this.navigateToCapture}
                >{I18n.t('title.addPicture')}</Button>
              </Row>
            </ListItem>
          </List>

        </Content>
        <View style={styles.fixedBottom}>
          <ButtonDone onPress={this.onSaveSensor}>Save Sensor</ButtonDone>
        </View>
        <GuidelineModal />
      </Container>
    )
  }
}

const InstallSensorScreen = connect(
  state => ({
    auth: state.auth,
    installs: state.installs
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallSensorScreenComponent)

export default InstallSensorScreen
