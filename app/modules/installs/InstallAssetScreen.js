import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Label } from 'native-base'
import { NavigationActions } from 'react-navigation'
import { TypeOrScan, Row, CustomSelect } from '../common/ui'
import { ThumbnailView } from '../common/CommonUI'
import { Button } from '../common/ui/Buttons'
import I18n from './locale'
import actions from './actions'
import styles from './styles'
import { navigateActionZoomable } from './utils'
import { CaptureTargetTypes } from './CaptureScreen'
import assetTypes, { other } from './constants/assetTypes'

class InstallAssetScreenComponent extends Component {

  navigateToCapture = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'CaptureScreen',
      params: CaptureTargetTypes.asset
    })
    this.props.navigateTo(navigateAction)
  }

  navigateToZoomable = (image) => () => this.props.navigateTo(navigateActionZoomable(image))

  render() {
    const { assetPicture, assetId, assetType: assetTypeField  } = this.props
    return (
      <Container style={styles.page}>
        <Content style={styles.content}>
          <CustomSelect
            label={I18n.t('installs.assetType')}
            placeholder={I18n.t('installs.selectAssetType')}
            textPlaceholder={I18n.t('installs.textAssetType')}
            field={assetTypeField}
            options={assetTypes}
            otherOption={other}
            onChange={this.props.actions.formAssetType}
            marginBottom={1}
            />
            <Label style={styles.label}>{I18n.t('installs.assetId')}</Label>
            <TypeOrScan style={styles.field}
            field={assetId}
            placeholder={I18n.t('installs.enterAssetId')}
            onChange={this.props.actions.formAssetId}
          />
          <Row center>
            <Button
              block
              icon="image"
              onPress={this.navigateToCapture}
            >{I18n.t('title.addPicture')}</Button>
          </Row>
          <Row center marginTop={0.5}>
            {assetPicture &&
            <ThumbnailView
              image={{uri:assetPicture}}
              onPress={this.navigateToZoomable}>
            </ThumbnailView>
            }
          </Row>
        </Content>
      </Container>
    )
  }
}

export default connect(
  state => ({
    assetId: state.installs.form.assetId,
    assetType: state.installs.form.assetType,
    assetPicture: state.installs.current.asset.picture
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallAssetScreenComponent)

