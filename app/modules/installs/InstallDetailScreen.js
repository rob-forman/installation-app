import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  View,
  Text
} from 'react-native'
import actions from './actions'

class InstallDetailScreenComponent extends Component {
  render() {
    return (
      <View><Text>InstallDetail Screen</Text></View>
    )
  }
}

const InstallDetailScreen = connect(
  state => ({
    auth: state.auth
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallDetailScreenComponent)

export default InstallDetailScreen
