import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ScrollView, View, Text, Modal, StyleSheet } from 'react-native'
import {
  IconButton,
} from '../common/CommonUI'
import I18n from './locale'
import actions from './actions'

const GuidelineModalComponent = props => (
  <Modal
    animationType="fade"
    transparent={true}
    visible={props.installs.isGuidelineVisible}
    onRequestClose={() => props.actions.hideGuideline()}
  >
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: '2%',
        padding: 5,
        height: '100%',
        backgroundColor: '#444444',
        borderWidth: 2,
        borderColor: '#888888',
        borderRadius: 8,
      }}
    >
      <View style={{ height: 60, flexDirection: 'row', flex: 1 }}>
        <View style={{ height: 60, width: '80%', paddingTop: 10 }}>
          <Text style={{ fontWeight: 'bold', color: '#ffffff', textAlign: 'center', marginLeft: 20}}>
            {I18n.t('installs.viewGuidelines')}
          </Text>
        </View>
        <View style={{ height: 50, width: '20%', }}>
          <IconButton
            size={16}
            backgroundColor="#444444"
            color="#ffffff"
            name="close"
            onPress={() => props.actions.hideGuideline()}
          />
        </View>

      </View>
      <View style={{height: '80%', width: '100%'}}>
      <ScrollView horizontal={true} style={{height: '100%', width: '100%'}}
      >
          <View style={{width: 400, height: 400, backgroundColor: '#eeeeee', margin: 5}}><Text style={{fontSize: 40, margin: 50}}>1...</Text></View>
          <View style={{width: 400, height: 400, backgroundColor: '#eeeeee', margin: 5}}><Text style={{fontSize: 40, margin: 50}}>2...</Text></View>
          <View style={{width: 400, height: 400, backgroundColor: '#eeeeee', margin: 5}}><Text style={{fontSize: 40, margin: 50}}>3...</Text></View>
      </ScrollView>
      </View>
    </View>
  </Modal>
)

const GuidelineModal = connect(
  state => ({
    installs: state.installs,
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch),
  }),
)(GuidelineModalComponent)

export default GuidelineModal

const styles = StyleSheet.create({

})
