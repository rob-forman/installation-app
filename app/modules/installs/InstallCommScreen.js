import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Label } from 'native-base'
import { NavigationActions } from 'react-navigation'
import { createSelector } from 'reselect'

import TypeOrScan from '../common/ui/TypeOrScan'
import { Button, ButtonGuide } from '../common/ui/Buttons'
import { Row } from '../common/ui/'
import I18n from './locale'
import actions from './actions'
import styles from './styles'
import { navigateActionZoomable } from './utils'
import { CaptureTargetTypes } from './CaptureScreen'
import { CustomSelect } from '../common/ui'
import commPlacementTypes, { other } from './constants/commPlacement'
import { SPACING, COLOR_LIGHT_GREY, COLOR_WHITE } from '../common/ui/constants'
import { ThumbnailView } from '../common/CommonUI'


class InstallCommScreenComponent extends Component {

  navigateToBarcodeScanner = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'ScanBarcode',
      params: 'CommUnit'
    })
    this.props.navigateTo(navigateAction)
  }

  navigateToCapture = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'CaptureScreen',
      params: CaptureTargetTypes.comm
    })
    this.props.navigateTo(navigateAction)
  }

  navigateToZoomable = (image) => () => this.props.navigateTo(navigateActionZoomable(image))

  onViewGuidelines = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'GuidelineManufacturers',
      params: 'commUnits'
    })
    this.props.navigateTo(navigateAction)
  }

  render() {
    const { placement, picture } = this.props.comm
    return (
      <Container style={styles.page}>
        <Row center padding={SPACING * 2} backgroundColor={COLOR_WHITE}>
          <ButtonGuide icon="book" block onPress={this.onViewGuidelines}>{I18n.t('installs.viewGuidelines')}</ButtonGuide>
        </Row>
        <Content style={styles.content}>
          <Label style={styles.label}>
            {I18n.t('installs.commId')}
          </Label>
          <TypeOrScan
            field={this.props.commId}
            placeholder={I18n.t('installs.enterCommId')}
            onChange={this.props.actions.formCommId}
            onScanPress={this.navigateToBarcodeScanner}
          />
          <CustomSelect
            label={I18n.t('installs.commPlacement')}
            placeholder={I18n.t('installs.commPlacementSelect')}
            textPlaceholder={I18n.t('installs.commPlacementText')}
            field={placement}
            options={commPlacementTypes}
            otherOption={other}
            onChange={this.props.actions.formCommPlacement}
            marginBottom={0.5}
          />
          <Row center>
            <Button
            block
              icon="image"
              onPress={this.navigateToCapture}
            >{I18n.t('title.addPicture')}</Button>
          </Row>
          <Row center marginTop={0.5}>
            <ThumbnailView image={{uri:picture}} onPress={this.navigateToZoomable}/>
          </Row>
        </Content>
      </Container>
    )
  }
}

const getComm = createSelector(
  state => state.installs.current.comm,
  (comm) => {
    let ret = {}
    for (const fieldName of Object.keys(comm)) {
      ret[fieldName] = ['picture'].includes(fieldName)
        ? comm[fieldName]
        : {value: comm[fieldName], error: null}
    }
    return ret
  }
)
export default connect(
  state => ({
    comm: getComm(state),
    commId: state.installs.form.commId
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch),
  }),
)(InstallCommScreenComponent)
