import React, { Component } from 'react'
import { View, Image } from 'react-native'
import I18n from './locale'

class InstallGuidelinesScreen extends Component {
  static navigationOptions = () => ({
    title: I18n.t('installs.guidelines')
  })

  render() {
    return (
      <View style={{
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Image
          source={require('../../../assets/img/comics.jpg')}
        />
      </View>
    )
  }
}

export default InstallGuidelinesScreen
