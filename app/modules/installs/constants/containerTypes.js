export const other = 'Other'
export default [
  'Carrier',
  'Dry',
  'Flat',
  'Open Top',
  'Reefer',
  'N/A',
  other
]
