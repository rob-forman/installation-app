import I18n from '../locale'

let sensorTypes = [
  { id: 'temperature', name: I18n.t('installs.sensorTypeTemperature')},
  { id: 'humidity', name: I18n.t('installs.sensorTypeHumidity')},

  { id: 'shock', name: I18n.t('installs.sensorTypeShock')},
  { id: 'motion', name: I18n.t('installs.sensorTypeMotion')},

  { id: 'light', name: I18n.t('installs.sensorTypeLight')},
  { id: 'door', name: I18n.t('installs.sensorTypeDoor')}
]

export default sensorTypes
