export default [
  {
    manufacturer: 'Carrier',
    type: 'ML3'
  }, {
    manufacturer: 'Carrier',
    type: 'ML2i'
  }, {
    manufacturer: 'Daikin',
    type: 'Zestia'
  }, {
    manufacturer: 'Daikin',
    type: 'Decos EFGH'
  }, {
    manufacturer: 'Daikin',
    type: 'Decos D'
  }, {
    manufacturer: 'Daikin',
    type: 'Decos C'
  }, {
    manufacturer: 'Starcool',
    type: 'CIM5'
  }, {
    manufacturer: 'Starcool',
    type: 'CIM6'
  }, {
    manufacturer: 'Starcool',
    type: 'CIM6.1'
  }, {
    manufacturer: 'TK',
    type: 'MP4000'
  }, {
    manufacturer: 'TK',
    type: 'MP3000'
  }, {
    manufacturer: 'TK',
    type: 'MP3000a'
  }
].map(item => `${item.manufacturer}: ${item.type}`)
