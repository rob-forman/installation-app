import I18n from '../locale'

let sensorLocations = [
  {
    id: 'INTERNAL_FRONT',
    name: I18n.t('installs.sensorLocationInternalFront')
  },
  {
    id: 'INTERNAL_MIDDLE',
    name: I18n.t('installs.sensorLocationInternalMiddle')
  },
  {
    id: 'INTERNAL_BACK',
    name: I18n.t('installs.sensorLocationInternalBack')
  },
  {
    id: 'AMBIENT_ENG',
    name: I18n.t('installs.sensorLocationAmbientEngSide')
  },
  {
    id: 'AMBIENT_DOOR',
    name: I18n.t('installs.sensorLocationAmbientDoorSide')
  },
  { id: 'OTHER', name: I18n.t('installs.sensorLocationOther') }
]

export const getSensorName = (locationId) => {
  let sensor = sensorLocations.find((sensorLocation) => sensorLocation.id == locationId)
  return sensor ? sensor.name : undefined
}

export default sensorLocations
