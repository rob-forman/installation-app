import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { createSelector } from 'reselect'
import { Icon } from 'native-base'
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Linking,
  Text,
  Alert
} from 'react-native'
import {
  DrawerHeaderLeft,
  DashButton,
  Logo,
} from '../common/CommonUI'
import { COLOR_BLUE, COLOR_WHITE, COLOR_LIGHT_GREY_BLUE } from '../common/ui/constants'
import actions from './actions'
import I18n from './locale'

const launchTestApp = () => {

  let testAppUrl = 'globeTrackerTestsApp://launch' // case insensitive
  Linking.canOpenURL(testAppUrl).then(supported => {
    if (!supported) {
      Alert.alert(I18n.t('installs.errorTestAppNotInstalled'))
    } else {
      return Linking.openURL(testAppUrl)
    }
  }).catch(err => Alert.alert(I18n.t('installs.errorUnknown'), err))
}

class InstallHomeScreenComponent extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: I18n.t('installs.navTitle'),
    headerStyle: {
    backgroundColor: COLOR_BLUE,
    },
    headerTintColor: '#fff',
    headerLeft: <DrawerHeaderLeft navigation={navigation} />
  })

  render() {
    const { navigate } = this.props.navigation
    return (
      <ScrollView style={styles.scrollView} contentContainerStyle={styles.container}>
        <DashButton
          title={I18n.t('installs.newInstall')}
          onPress={() => {
            this.props.actions.resetCurrent()
            navigate('Install')
          }}
          image={require('../../../assets/img/icon_new_install.png')}
        />
        <DashButton
          title={I18n.t('installs.existingInstalls')}
          onPress={() => navigate('InstallList')}
            image={require('../../../assets/img/icon_list_install.png')}
        >
          { this.props.installsToUpload > 0 ?
            <View style={[styles.installs, styles.filled]}>
              <Text style={styles.installsText}>
                {this.props.installsToUpload}
              </Text>
            </View>
            :
            <View style={styles.installs}>
              <Icon name="cloud-done" style={cloudIcon} />
            </View>
          }
        </DashButton>
        <DashButton
          title={I18n.t('installs.launchTest')}
          onPress={launchTestApp}
          image={require('../../../assets/img/icon_test_launch.png')}
        />
      </ScrollView>
    )
  }
}

const getNonUploadedInstallsCount = createSelector(
  state => state.installs.installCommands,
  commands => commands
    .reduce((memo, cmd) => cmd.isUploaded ? memo : memo + 1 , 0)
)

export default connect(
  state => ({
    auth: state.auth,
    installs: state.installs,
    installsToUpload: getNonUploadedInstallsCount(state)
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(InstallHomeScreenComponent)

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#e8ebf0',
  },
  container: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: COLOR_LIGHT_GREY_BLUE,
    padding: 20,
  },
  installs: {
    position: 'absolute',
    top: -12,
    right: -6,
    width: 24,
    height: 24,
    borderWidth: 0,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filled: {
    backgroundColor: COLOR_BLUE,
  },
  installsText: {
    color: COLOR_WHITE,
    fontWeight: 'bold',
    fontSize: 12,
  }
})

const cloudIcon = {
  color: COLOR_BLUE,
  fontSize: 24
}
