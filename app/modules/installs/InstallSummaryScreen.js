import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text } from 'react-native'
import {
  Container,
  Content,
  Label,
  List,
  ListItem,
  Body,
  Icon,
  Separator,
  Text as BaseText
} from 'native-base'
import { ThumbnailView } from '../common/CommonUI'
import styles from './styles'
import { COLOR_BLUE, COLOR_DARK_GREY} from '../common/ui/constants'
import { SensorIcon } from '../common/ui/Icons'
import I18n from './locale'
import { navigateActionZoomable } from './utils'
import {getSensorName} from './constants/sensorLocations'

const center = { alignItems: 'center' }
const centerText = { textAlign: 'center' }
const left = { justifyContent: 'flex-start' }

class InstallSummaryScreen extends Component {

  navigateToZoomable = (image) => () => this.props.navigateTo(navigateActionZoomable(image))

  renderCompany(){
    const {
      companyId,
      companies
    } = this.props

    if(!companyId) {
      return <ListItemEmpty>{I18n.t('installsSummary.noCompanyId')}</ListItemEmpty>
    }

    return (
      <ListItem last>
        <Body>
          <BaseText>{I18n.t('installsSummary.id')}</BaseText>
          <BaseText note>{companies.find((company) => company.id == companyId).name}</BaseText>
        </Body>
      </ListItem>
    )
  }

  renderAsset() {
    const {
      assetId,
      assetType,
      assetPicture
    } = this.props

    if(!assetId) {
      return <ListItemEmpty>{I18n.t('installsSummary.noAssetId')}</ListItemEmpty>
    }

    if(!assetType) {
      return <ListItemEmpty>{I18n.t('installsSummary.noAssetType')}</ListItemEmpty>
    }

    return (
      <ListItem last>
        <ThumbnailView image={{uri:assetPicture}} onPress={this.navigateToZoomable}/>
        <Body>
          <BaseText>{I18n.t('installsSummary.id')}</BaseText>
          <BaseText note>{assetId}</BaseText>
        </Body>
        <Body>
          <BaseText>{I18n.t('installsSummary.type')}</BaseText>
          <BaseText note>{assetType}</BaseText>
        </Body>
      </ListItem>
    )
  }

  renderCommUnit() {
    const {
      commId,
      commUnitPicture,
      commUnitPlacement
    } = this.props

    if(!commId) {
      return <ListItemEmpty>{I18n.t('installsSummary.noCommUnitId')}</ListItemEmpty>
    }

    return (
      <ListItem last>
        <ThumbnailView image={{uri: commUnitPicture}} onPress={this.navigateToZoomable}/>
        <Body>
          <BaseText>{I18n.t('installsSummary.id')}</BaseText>
          <BaseText note>{commId}</BaseText>
        </Body>
        {commUnitPlacement &&
        <Body>
          <BaseText>{I18n.t('installsSummary.commUnitPlacement')}</BaseText>
          <BaseText note>{commUnitPlacement}</BaseText>
        </Body>
        }
      </ListItem>

    )
  }

  renderSensors() {
    const { sensors } = this.props

    if (!sensors.length) {
      return <ListItemEmpty>{I18n.t('installsSummary.noSensors')}</ListItemEmpty>
    }

    return sensors.map(sensor => [(
      <ListItem key={sensor.id} itemDivider>
        <Body>
          <BaseText>{I18n.t('installsSummary.id')}</BaseText>
          <BaseText note>{sensor.id || '-'}</BaseText>
        </Body>
      </ListItem>
    ), (
      <ListItem key={sensor.id + 'type'} last={!sensor.pictures.length}>
        <Body>
          <BaseText>{I18n.t('installsSummary.type')}</BaseText>
          <BaseText note>
          { Object.keys(sensor.type).map(type =>
              <SensorIcon key={type} type={type} size={24} color={COLOR_DARK_GREY} />)|| '-'
          }
          </BaseText>
        </Body>
        <Body>
          <BaseText>{I18n.t('installsSummary.location')}</BaseText>
          <BaseText note>{getSensorName(sensor.location) || '-'}</BaseText>
        </Body>
      </ListItem>
    ), ( sensor.pictures.length ?
      <ListItem last style={left}>
        { sensor.pictures.map((uri, idx) => (
          <ThumbnailView key={idx}  style={styles.marginRight}
            image={{uri}} onPress={this.navigateToZoomable}/>
        ))}
      </ListItem> : null
    )])
  }

  render() {
    return (
      <Container style={styles.page}>
        <Content style={styles.contentNoPadding}>
          <List>
            <Separator style={styles.separator} bordered>
              <Label style={styles.label}>{I18n.t('installsSummary.company')}</Label>
            </Separator>
            {this.renderCompany()}
            <Separator style={styles.separator} bordered>
              <Label style={styles.label}>{I18n.t('installsSummary.asset')}</Label>
            </Separator>
            {this.renderAsset()}
            <Separator style={styles.separator} bordered>
              <Label style={styles.label}>{I18n.t('installsSummary.commUnit')}</Label>
            </Separator>
            {this.renderCommUnit()}
            <Separator style={styles.separator} bordered>
              <Label style={styles.label}>{I18n.t('installsSummary.sensors')}</Label>
            </Separator>
            {this.renderSensors()}
            <ListItem last>
              <Body>
                <View style={center}><Icon name="information-circle" style={{ color: COLOR_BLUE }} /></View>
                <View>
                  <Text style={centerText}>
                    {I18n.t('installsSummary.note')}
                  </Text>
                </View>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

export default connect(
  state => ({
    companyId:  state.installs.form.companyId.value,
    companies: state.dataFetch.companies,
    sensors: state.installs.current.sensors,
    assetId: state.installs.form.assetId.value,
    assetType: state.installs.form.assetType.value,
    commId: state.installs.form.commId.value,
    assetPicture: state.installs.current.asset.picture,
    commUnitPicture: state.installs.current.comm.picture,
    commUnitPlacement: state.installs.current.comm.placement
  })
)(InstallSummaryScreen)

const ListItemEmpty = ({ children }) =>
  <ListItem last>
    <Body>
      <BaseText note>{children}</BaseText>
    </Body>
  </ListItem>
