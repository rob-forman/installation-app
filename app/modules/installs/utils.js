import { NavigationActions } from 'react-navigation'

export function* getImageUrisFromInstall(install) {
  if (install.asset.picture) {
    yield install.asset.picture
  }

  if (install.comm.picture) {
    yield install.comm.picture
  }

  for (const { pictures } of install.sensors) {
    for (const picture of pictures) {
      yield picture
    }
  }
}

export function* getImageUrisFromInstallCommands(commands) {
  for (const { install } of commands) {
    for (const image of getImageUrisFromInstall(install)) {
      yield image
    }
  }
}

export const extractImageUrisFromInstallCommands = commands => (
  new Set(getImageUrisFromInstallCommands(commands))
)

export const isImageUriStoredInInstallCommands = (commands, imageUri) => {
  for (const uri of commands) {
    if (uri == imageUri) {
      return true
    }
  }
  return false
}

export const navigateActionZoomable = image => (
  NavigationActions.navigate({
    routeName: 'ZoomableScreen',
    params: image
  })
)
