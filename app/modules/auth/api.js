import {base64} from '../common/utils'
import {Alert} from 'react-native'

const baseUrl = 'https://gtdev1.globetracker.com/api/v0/gt-install'
const mockUrl = 'http://share.salsitasoft.com:8080'

const oauthTokenUrl = baseUrl + '/oauth/token'
const companiesUrl = baseUrl + '/customers'
const assetUrl = baseUrl + '/assets'
const sensorsUrl = baseUrl + '/sensors'
const installsUrl = baseUrl + '/installs'
export const imageUrl = baseUrl + '/images'

const clientId = 'gt-install-mobile'
const clientSecret = 'gt-install-dev'

// I cannot use inheritance on Errors, since babel doesn't support it,
// therefore I'm using flags https://babeljs.io/docs/usage/caveats/#classes
export const Causes = {
  NetworkErrorCause: 'NETWORK_ERROR_CAUSE',
  InvalidTokenCause: 'INVALID_TOKEN_CAUSE',
  Not200ResultOtherCause: 'NOT_200_CAUSE',
  InvalidGrant: 'INVALID_GRANT'
}

const getInitialAuthHeader = (clientId, clientSecret) => ({
  'Authorization': 'Basic ' + base64.btoa(clientId + ':' + clientSecret)
})

const getAuthHeaderForToken = (authToken) => ({
  'Authorization': 'Bearer ' + authToken // note that it's a bearer not a basic
})

export const fetchAuthToken = ({username, password}) => {
  const qs = `client_id=${clientId}&grant_type=password&password=${password}&username=${username}`

  // result example:
  // {
  //   access_token: <string>,
  //   refresh_token: <string>,
  //   expires_in: <seconds, int>,
  //   token_type: bearer
  // }
  return fetch(`${oauthTokenUrl}?${qs}`, {
    method: 'post',
    headers: getInitialAuthHeader(clientId, clientSecret)
  })
  .catch(e => {
    const networkError = new Error(e)
    networkError.cause = Causes.NetworkErrorCause
    throw networkError
  })
  .then(res => {
    if (res.status == 200) {
      return res.json()
    } else {
      return res.json().then(json => {
        const e = new Error(json)
        e.cause = (json.error === 'invalid_grant' ? Causes.InvalidGrant : Causes.Not200ResultOtherCause)
        throw e
      })
    }
  })
}

export const fetchAuthTokenFromRefreshToken = (refreshToken) => {
  const qs = `client_id=${clientId}&grant_type=refresh_token&refresh_token=${refreshToken}`
  // result example:
  // {
  //   access_token: <string>,
  //   refresh_token: <string>,
  //   expires_in: <seconds, int>,
  //   token_type: bearer
  // }
  return fetch(`${oauthTokenUrl}?${qs}`, {
    method: 'post',
    headers: getInitialAuthHeader(clientId, clientSecret)
  })
  .catch(e => {
    const error = new Error(e.message)
    error.cause = Causes.NetworkErrorCause
    throw error
  })
  .then(res => {
    if (res.status == 200) {
      return res.json()
    } else {
      return res.json().then(json => {
        if (json.error == 'invalid_token') {
          const e = new Error(json.error_description)
          e.cause = Causes.InvalidTokenCause
          throw e
        } else {
          const e = new Error(JSON.stringify(json))
          e.cause = Causes.Not200ResultOtherCause
          throw e
        }
      })
    }
  })
}

export const fetchCompanies = ({accessToken}) => {
  // returns something like [
  //   {id: 1, name: 'Company A'},
  //   {id: 2, name: 'Company B'},
  // ]
  return fetch(companiesUrl, {
    method: 'get',
    headers: getAuthHeaderForToken(accessToken)
  }).then(res => {
    if (res.status == 200) {
      return res.json()
    } else {
      throw new Error('!Company fetch error')
    }
  })
}

export const fetchAssets = ({accessToken}) => {
  // results example: [{
  //   "id": "EIMU469268-8",
  //   "owner_id": "GT.DEV1",
  //   "type": "unknown"
  // }]
  return fetch(assetUrl, {
    method: 'get',
    headers: getAuthHeaderForToken(accessToken)
  }).then(res => {
    if (res.status == 200) {
      return res.json()
    } else {
      throw new Error('Asset fetch error')
    }
  })
}

export const fetchSensors = ({accessToken}) => {
  return fetch(sensorsUrl, {
    method: 'get',
    headers: getAuthHeaderForToken(accessToken)
  }).then(res => {
    if (res.status == 200) {
      return res.json()
    } else {
      throw new Error('Sensor fetch error')
    }
  })
}

export const postInstall = (accessToken, install) => {

  let customFields = install.customFields
  let myCustomFields = {}
  for (const fieldName in customFields) {
    const val = customFields[fieldName]
    if (typeof val === 'string' && val != '') {
      myCustomFields[fieldName] = val
    }
  }

  const requestBody = {
    id: '', // install.id,
    asset: {
      id: install.asset.id,
      owner_id: install.company.id,
      type: install.asset.type,
      custom_fields: myCustomFields
    },
    boxes: [ // =commUnits
      {
        id: install.comm.id,
        owner_id: install.company.id,
        asset_id: install.asset.id,
        custom_fields: {
          placement: install.comm.placement
        }
      }
    ],
    sensors: install.sensors.map(sensor => ({
      id: sensor.id,
      owner_id: install.company.id,
      box_id: install.comm.id,
      custom_fields: {
        location: sensor.location,
      },
      type: Object.keys(sensor.type).join(',')
    }))
  }

  return fetch(installsUrl, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      ...getAuthHeaderForToken(accessToken)
    },
    body: JSON.stringify(requestBody)
  }).then(res => {
    let resPromise = res.json()
    if (res.status == 200) {
      return
    } else {
      resPromise.then(json => {
        if(json.message) {
          Alert.alert(`Error ${res.status}: ${json.message}`)
        } else {
          Alert.alert(`Error ${res.status}: unexpected server response`)
        }
      })
      throw new Error('!Post installs error')
    }
  })
}
