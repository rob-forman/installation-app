import I18n from '../../locale/setup'

I18n.translations.en.auth = {}
I18n.translations.en.auth.login = {
  navTitle: 'Globe Tracker',
  loginButton: 'Login',
  needAccount: 'Need an account?',
  contact: 'Contact',
  skip: 'Skip',
  incorrectCredentials: 'Incorrect username/password'
}
I18n.translations.en.auth.logout = {
  logoutButton: 'Log out'
}


I18n.translations.es.auth = {}
I18n.translations.es.auth.login = {
  navTitle: 'Globe Tracker',
  loginButton: 'Iniciar Sesión',
  needAccount: '¿Necesito una cuenta?',
  contact: 'Contactar',
  skip: 'Omitir',
  incorrectCredentials: 'Usuario/contraseña incorrectos'
}

I18n.translations.es.auth.logout = {
  logoutButton: 'Salir'
}


export default I18n
