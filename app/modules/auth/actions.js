import { isBlank } from '../../tools/Validation'
import I18n from './locale'

const ns = 'AUTH'
export const types = {
  RESET: ns+'/RESET',

  BOOT_PENDING: ns + '/BOOT_PENDING',
  BOOT_FINISH: ns + '/BOOT_FINISH',
  BOOT_FINISH_LOAD_CACHE: ns + '/BOOT_FAILURE_LOAD_CACHE',

  FORM_USERNAME_PENDING: ns + '/FORM_USERNAME_PENDING',
  FORM_USERNAME_SUCCESS: ns + '/FORM_USERNAME_SUCCESS',
  FORM_USERNAME_FAILURE: ns + '/FORM_USERNAME_FAILURE',
  FORM_PASSWORD_PENDING: ns + '/FORM_PASSWORD_PENDING',
  FORM_PASSWORD_SUCCESS: ns + '/FORM_PASSWORD_SUCCESS',
  FORM_PASSWORD_FAILURE: ns + '/FORM_PASSWORD_FAILURE',

  LOGIN_PENDING: ns + '/LOGIN_PENDING',
  LOGIN_SUCCESS: ns + '/LOGIN_SUCCESS',
  LOGIN_FAILURE: ns + '/LOGIN_FAILURE',

  REFRESH_SUCCESS: ns + '/REFRESH_SUCCESS',

  LOGOUT_PENDING: ns + '/LOGOUT_PENDING',
  LOGOUT_SUCCESS: ns + '/LOGOUT_SUCCESS',
  LOGOUT_FAILURE: ns + '/LOGOUT_FAILURE',

  CACHE_LOADED: ns + '/CACHE_LOADED',
  RESTORE_FROM_CACHE: ns + '/RESTORE_FROM_CACHE',
}

// Validations
const usernameFailure = (username) => {
  if (isBlank(username)) {
    return {
      type: types.FORM_USERNAME_FAILURE,
      value: username,
      error: I18n.t('common.required')
    }
  }
}


const passwordFailure = (password) => {
  if (isBlank(password)) {
    return {
      type: types.FORM_PASSWORD_FAILURE,
      value: password,
      error: I18n.t('common.required')
    }
  }
}

// Actions
const reset = () => ({type: types.RESET})

const formUsername = (nextUsername) => dispatch => {
  dispatch({type: types.FORM_USERNAME_PENDING})

  let failure = usernameFailure(nextUsername)
  if (failure) {
    dispatch(failure)
  } else {
    dispatch({
      type: types.FORM_USERNAME_SUCCESS,
      value: nextUsername,
    })
  }
}

const formPassword = (nextPassword) => dispatch => {
  dispatch({type: types.FORM_PASSWORD_PENDING})

  let failure = passwordFailure(nextPassword)
  if (failure) {
    dispatch(failure)
  } else {
    dispatch({
      type: types.FORM_PASSWORD_SUCCESS,
      value: nextPassword,
    })
  }
}

const login = (username, password) => dispatch => {
  dispatch({type: types.LOGIN_PENDING, username, password})
}

const logout = () => dispatch => {
  dispatch({
    type: types.LOGOUT_PENDING
  })

  dispatch({
    type: types.LOGOUT_SUCCESS
  })
}

const boot = () => dispatch => {
  dispatch({
    type: types.BOOT_PENDING
  })
}

const restoreFromCache = (cache) => dispatch => {
  dispatch({
    type: types.RESTORE_FROM_CACHE,
    value: cache
  })
}

const actions = {
  reset,
  formUsername,
  formPassword,
  login,
  logout,
  boot,
  restoreFromCache
}

export default actions
