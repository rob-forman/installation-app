import { types } from './actions'
import { reduceFormField } from '../../tools/reducers'

const initialState = {
  isBooting: false,

  // example of authenticatedUser
  // {
  //   username: action.username,
  //   accessToken: action.accessToken,
  //   refreshToken: action.refreshToken,
  //   expirationDate: action.expirationDate
  // }
  authenticatedUser: null,
  isPending: false,
  error: null,
  form: {
    username: { value: '', error: null },
    password: { value: '', error: null },
  },
  cache: null
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.BOOT_PENDING:
    return { ...state, isBooting: true}

  case types.BOOT_FINISH_LOAD_CACHE:
  case types.BOOT_FINISH:
    return { ...state, isBooting: false}

  case types.RESET:
    return initialState

  case types.FORM_USERNAME_PENDING:
    return reduceFormField('username', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_USERNAME_SUCCESS:
    return reduceFormField('username', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_USERNAME_FAILURE:
    return {
      ...state,
      user: null,
      ...reduceFormField('username', state, {
        value: action.value,
        error: action.error,
      }),
    }

  case types.FORM_PASSWORD_PENDING:
    return reduceFormField('password', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_PASSWORD_SUCCESS:
    return reduceFormField('password', state, {
      value: action.value,
      error: null,
    })
  case types.FORM_PASSWORD_FAILURE:
    return reduceFormField('password', state, {
      value: action.value,
      error: action.error,
    })

  case types.LOGIN_PENDING:
    return { ...state, isPending: true, authenticatedUser: null, error: null }

  case types.REFRESH_SUCCESS:
    return {
      ...state,
      authenticatedUser: {
        ...state.authenticatedUser, // to keep the username
        ...action.value
      }
    }
  case types.LOGIN_SUCCESS:
    return {
      ...state,
      isPending: false,
      authenticatedUser: action.value
    }

  case types.LOGIN_FAILURE:
    return {
      ...state,
      isPending: false,
      authenticatedUser: null,
      error: action.error,
    }

  case types.LOGOUT_PENDING:
    return { ...state, isPending: true, error: null }
  case types.LOGOUT_SUCCESS:
    return { ...state, isPending: false, authenticatedUser: null }
  case types.LOGOUT_FAILURE:
    return { ...state, isPending: false, error: action.error }

  case types.CACHE_LOADED:
    return { ...state, cache: action.value }

  default:
    return state
  }
}
