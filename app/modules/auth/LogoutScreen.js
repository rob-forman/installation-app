import { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from './actions'
import I18n from './locale'

// This component exists to simplify logging out
// by navigating to Logout.
class LogoutScreenComponent extends Component {
  constructor(props) {
    super(props)
  }

  static navigationOptions = () => ({
    title: I18n.t('auth.logout.logoutButton')
  })
  componentWillMount() {
    this.props.actions.logout()
  }

  render() { return null }
}

const LogoutScreen = connect(
  state => ({
    auth: state.auth
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(LogoutScreenComponent)

export default LogoutScreen
