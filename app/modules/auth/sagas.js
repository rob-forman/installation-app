import { AsyncStorage } from 'react-native'
import { all, call, put, takeEvery } from 'redux-saga/effects'
import { types as authTypes } from './actions'
import { types as dataFetchTypes } from '../dataFetch/actions'
import { fetchAuthToken,
  fetchAuthTokenFromRefreshToken,
  fetchCompanies,
  fetchAssets,
  Causes
} from './api'
import I18n from './locale'

const StorageKeys = {
  assets: 'cache.assets',
  companies: 'cache.companies',
  authenticatedUser: 'cache.authenticatedUser',
  username: 'cache.username'
}

function* fetchAssetsAndCompanies(accessToken) {
  try {
    const companies = yield call(fetchCompanies, {accessToken: accessToken})
    yield put({type: dataFetchTypes.FETCH_COMPANIES_SUCCESS, value: companies})

    const assets = yield call(fetchAssets, {accessToken: accessToken})
    yield put({type: dataFetchTypes.FETCH_ASSETS_SUCCESS, value: assets})
  } catch (err) {
    console.log(err)
  }
}

function* onBootstrap() {
  const authenticatedUser = JSON.parse(yield call(AsyncStorage.getItem, StorageKeys.authenticatedUser))

  if (!authenticatedUser) { // no user, jump to login screen
    console.log('no user saved, login')
    yield put({
      type: authTypes.BOOT_FINISH
    })
    return
  }

  try {
    yield call(tryRefreshAccessToken, authenticatedUser.refreshToken)
  } catch(e) {
    const expirationDate = authenticatedUser.expirationDate
    switch (e.cause) {
    case Causes.NetworkErrorCause:
      if (Date.now() > expirationDate) { // expired
        yield put({
          type: authTypes.BOOT_FINISH
        })
      } else { // still can use data, restore from storage
        yield put({
          type: authTypes.BOOT_FINISH_LOAD_CACHE
        })
      }
      return

    case Causes.InvalidTokenCause:
      yield put({
        type: authTypes.BOOT_FINISH
      })
      return

    default:
      yield put({
        type: authTypes.BOOT_FINISH
      })
      return
    }

  }

  // everything is fine, update assets & companies and login
  yield call(fetchAssetsAndCompanies, authenticatedUser.accessToken)
  yield put({
    type: authTypes.BOOT_FINISH
  })
  yield put({
    type: authTypes.LOGIN_SUCCESS,
    value: authenticatedUser
  })
}

function* onBootFailure() {
  const assets = JSON.parse(yield call(AsyncStorage.getItem, StorageKeys.assets))
  const companies = JSON.parse(yield call(AsyncStorage.getItem, StorageKeys.companies))
  const authenticatedUser = JSON.parse(yield call(AsyncStorage.getItem, StorageKeys.authenticatedUser))

  if (assets && companies && authenticatedUser) {
    yield put({
      type: authTypes.CACHE_LOADED,
      value: {
        assets,
        companies,
        authenticatedUser
      }
    })
  }
}

function* onRestoreFromCache({ value }) {
  yield put({type: dataFetchTypes.FETCH_COMPANIES_SUCCESS, value: value.companies})
  yield put({type: dataFetchTypes.FETCH_ASSETS_SUCCESS, value: value.assets})
  yield put({
    type: authTypes.LOGIN_SUCCESS,
    value: value.authenticatedUser
  })
}
export function* tryRefreshAccessToken(refreshToken) {
  const res = yield call(fetchAuthTokenFromRefreshToken, refreshToken)
  yield put({
    type: authTypes.REFRESH_SUCCESS,
    value: {
      accessToken: res.access_token,
      refreshToken: res.refresh_token,
      expirationDate: Date.now() + (1000 * res.expires_in),
    }
  })
}

function* onLogin({username, password}) {
  try {
    const res = yield call(fetchAuthToken, {username, password})
    if (res.access_token) {
      yield fetchAssetsAndCompanies(res.access_token)

      yield put({
        type: authTypes.LOGIN_SUCCESS,
        value: {
          username: username,
          accessToken: res.access_token,
          refreshToken: res.refresh_token,
          expirationDate: Date.now() + (1000 * res.expires_in)
        }
      })
    } else {
      yield put({type: authTypes.LOGIN_FAILURE, error: I18n.t('auth.login.incorrectCredentials')})
    }
  } catch (e) {
    yield put({type: authTypes.LOGIN_FAILURE,
      error: e.cause === Causes.InvalidGrant ? I18n.t('auth.login.incorrectCredentials') : e.message})
  }
}

function* onLogoutSuccess() {
  for (const key in StorageKeys) {
    if (StorageKeys.hasOwnProperty(key)) {
      yield call(AsyncStorage.removeItem, StorageKeys[key])
    }
  }
  yield put({
    type: authTypes.RESET
  })
}

function* setAsyncStorageItem(key, value) {
  yield call(AsyncStorage.setItem, key, value)
}

function* setAssets(action) {
  yield call(setAsyncStorageItem, StorageKeys.assets, JSON.stringify(action.value))
}

function* setCompanies(action) {
  yield call(setAsyncStorageItem, StorageKeys.companies, JSON.stringify(action.value))
}

function* setAuthenticatedUser(action) {
  yield call(setAsyncStorageItem, StorageKeys.authenticatedUser, JSON.stringify(action.value))
}

export default function* loginSaga() {
  yield all([
    takeEvery(authTypes.BOOT_PENDING, onBootstrap),
    takeEvery(authTypes.BOOT_FINISH_LOAD_CACHE, onBootFailure),
    takeEvery(authTypes.LOGIN_PENDING, onLogin),
    takeEvery(authTypes.LOGOUT_SUCCESS, onLogoutSuccess),
    takeEvery(authTypes.RESTORE_FROM_CACHE, onRestoreFromCache),
    takeEvery(authTypes.LOGIN_SUCCESS, setAuthenticatedUser),
    takeEvery(dataFetchTypes.FETCH_ASSETS_SUCCESS, setAssets),
    takeEvery(dataFetchTypes.FETCH_COMPANIES_SUCCESS, setCompanies),
  ])
}
