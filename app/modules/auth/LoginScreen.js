import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  ActivityIndicator,
  Button,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native'
import {
  CommonStyle,
  Logo,
  ActionButton,
  Link,
  TextInputField } from '../common/CommonUI'
import I18n from './locale'
import actions from './actions'

const LoadingComponent = () => (
  <ScrollView style={styles.scrollView} contentContainerStyle={styles.container}>
    <Logo />
    <View style={{marginTop: 20, width: '100%'}}>
    </View>
    <View style={{width: '100%', flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
      <ActivityIndicator size="large" color="#007dd7" animating={true} />
    </View>
  </ScrollView>
)

class LoginComponent extends Component {

  onPressSkipButton = () => {
    this.props.actions.restoreFromCache(this.props.auth.cache)
  }

  render() {
    let { auth, actions } = this.props
    let { formUsername, formPassword, login } = actions
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        style={styles.scrollView}
        contentContainerStyle={styles.container}>
        <Logo />
        <View style={{marginTop: 20, width: '100%'}}>
          <Text style={[CommonStyle.errorText, {textAlign: 'center', width: '100%'}]}>{this.props.auth.error}</Text>
          <TextInputField
            ref={ref => this.usernameField = ref }
            field={auth.form.username}
            style={CommonStyle.textInput}
            placeholder="Username"
            onChangeText={text => formUsername(text)}
            returnKeyType="next"
            onSubmitEditing={() => {
              formUsername(this.usernameField.input._lastNativeText)
              this.passwordField.input.focus()
            }}/>
          <TextInputField
            ref={ref => this.passwordField = ref }
            field={auth.form.password}
            style={CommonStyle.textInput}
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={text => formPassword(text)}
            returnKeyType="go"
            onSubmitEditing={() => {
              formPassword(this.passwordField.input._lastNativeText)
              login(
                this.usernameField.input._lastNativeText,
                this.passwordField.input._lastNativeText)
            }}/>
        </View>
        <View style={{width: '100%', flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
          <ActionButton
            title={I18n.t('auth.login.loginButton')}
            isPending={this.props.auth.isPending}
            onPress={() => login(this.props.auth.form.username.value, this.props.auth.form.password.value)} />

          {this.props.auth.cache ? <Button onPress={this.onPressSkipButton} title={I18n.t('auth.login.skip')} color="#007aff" /> : null }

          <View style={{width: '100%',flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
            <Text>{I18n.t('auth.login.needAccount')}</Text>
            <View style={{marginTop: 10, flex: 1, flexDirection: 'row', alignItems: 'flex-start'}}>
              <Text>{I18n.t('auth.login.contact')} </Text>
              <Link
                title='support@globetracker.com'
                url='mailto:support@globetracker.com?&subject=Globe Tracker Installation App Account Request' />
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

class LoginScreenComponent extends Component {

  render() {
    if (this.props.auth.isBooting) {
      return <LoadingComponent />
    } else {
      return <LoginComponent auth={this.props.auth} actions={this.props.actions} />
    }
  }
}

const LoginScreen = connect(
  state => ({
    auth: state.auth
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(LoginScreenComponent)

export default LoginScreen

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#ffffff',
  },
  container: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    padding: '10%',
    paddingTop: '20%',
  }
})
