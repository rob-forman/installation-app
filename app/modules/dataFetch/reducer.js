import { types } from './actions'

const initialState = {
  isPending: false,
  companies: [],
  assets: []
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.FETCH_COMPANIES_PENDING:
    return { ...state, isPending: true, companies: [], error: null }
  case types.FETCH_COMPANIES_SUCCESS: {
    return { ...state, isPending: false, companies: action.value }
  }
  case types.FETCH_COMPANIES_FAILURE:
    return { ...state, isPending: false, companies: [], error: action.error }
  case types.FETCH_ASSETS_SUCCESS:
    return { ...state, assets: action.value}
  default:
    return state
  }
}

