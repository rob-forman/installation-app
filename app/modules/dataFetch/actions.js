

const ns = 'DATA_FETCH'
export const types = {
  RESET: ns + '/RESET',

  FETCH_COMPANIES_PENDING: ns + '/FETCH_COMPANIES_PENDING',
  FETCH_COMPANIES_SUCCESS: ns + '/FETCH_COMPANIES_SUCCESS',
  FETCH_COMPANIES_FAILURE: ns + '/FETCH_COMPANIES_FAILURE',

  FETCH_ASSETS_SUCCESS: ns + '/FETCH_ASSETS_SUCCESS'

}

// Actions
const reset = () => ({type: types.RESET})


const actions = {
  reset
}

export default actions
