import I18n from '../../locale/setup'

I18n.translations.en.zoomable = {
  detail: 'Detail',
}

I18n.translations.es.zoomable = {
  detail: 'Detalle',
}


export default I18n
