export const isAssetIdValid = (con) => ISO6346Check(con)

// https://en.wikipedia.org/wiki/ISO_6346#Code_Sample_.28Javascript.29
export const ISO6346Check = (con) => {
  if (!con || con == '' || con.length != 11) { return false }
  con = con.toUpperCase()
  let re = /^[A-Z]{4}\d{7}/
  if (re.test(con)) {
    let sum = 0
    for (let i = 0; i < 10; i++) {
      let n = con.substr(i, 1)
      if (i < 4) {
        n = '0123456789A?BCDEFGHIJK?LMNOPQRSTU?VWXYZ'.indexOf(con.substr(i, 1))
      }
      n *= Math.pow(2, i)
      sum += n
    }
    if (con.substr(0, 4) == 'HLCU') {
      sum -= 2
    }
    sum %= 11
    sum %= 10
    return sum == con.substr(10)
  } else {
    return false
  }
}

// https://stackoverflow.com/questions/42829838/react-native-atob-btoa-not-working-without-remote-js-debugging
// Inspired by: https://github.com/davidchambers/Base64.js/blob/master/base64.js
const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
export const base64 = {
  btoa: (input = '')  => {
    let str = input
    let output = ''

    for (let block = 0, charCode, i = 0, map = chars;
    str.charAt(i | 0) || (map = '=', i % 1);
    output += map.charAt(63 & block >> 8 - i % 1 * 8)) {

      charCode = str.charCodeAt(i += 3/4)

      if (charCode > 0xFF) {
        throw new Error("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");
      }

      block = block << 8 | charCode
    }

    return output
  },

  atob: (input = '') => {
    let str = input.replace(/=+$/, '')
    let output = ''

    if (str.length % 4 == 1) {
      throw new Error("'atob' failed: The string to be decoded is not correctly encoded.")
    }
    for (let bc = 0, bs = 0, buffer, i = 0;
      buffer = str.charAt(i++);
      ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
        bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
    ) {
      buffer = chars.indexOf(buffer)
    }

    return output
  }
}
// https://jsfiddle.net/briguy37/2MVFd/
export const generateUUID = () => {
  var d = new Date().getTime()
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = (d + Math.random()*16)%16 | 0
    d = Math.floor(d/16)
    return (c=='x' ? r : (r&0x3|0x8)).toString(16)
  })
}

export const generateId = () => (Math.floor(Math.random() * Number.MAX_SAFE_INTEGER))
