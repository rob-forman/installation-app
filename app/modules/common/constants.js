import RNFS from 'react-native-fs'

export const SENSOR_TYPES = {
  temperature: 'thermometer-lines',
  humidity: 'water-percent',
  motion: 'cube-outline',
  shock: 'blur',
  light: 'white-balance-sunny',
  door: 'glassdoor',
}

export const imagesDirectory = `${RNFS.DocumentDirectoryPath}/images`
