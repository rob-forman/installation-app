import React, { Component } from 'react'
import {
  Image,
} from 'react-native'

import ImageZoom from 'react-native-image-pan-zoom'
import dimensions from 'Dimensions'

import I18n from './locale'

class ZoomableScreen extends Component {

  static navigationOptions = () => ({
    title: I18n.t('zoomable.detail')
  })

  render() {
    const image = this.props.navigation.state.params
    const { width, height } = dimensions.get('window')
    const style = { width, height }
    return (
      <ImageZoom
        cropWidth={style.width}
        cropHeight={style.height}
        imageWidth={style.width}
        imageHeight={style.height}
      >
        <Image style={style} source={image}/>
      </ImageZoom>
    )
  }
}

export default ZoomableScreen
