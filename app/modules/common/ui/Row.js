import React from 'react'
import { View } from 'react-native'
import { BASELINE } from '../ui/constants'

export default function Row ({
  children,
  center,
  marginTop = 0,
  marginBottom = 0,
  ...rest
}) {
  const style = {
    justifyContent: center ? 'center' : 'flex-start',
    marginTop: marginTop * BASELINE,
    marginBottom: marginBottom * BASELINE,
    ...rest,
  }

  return (
    <View style={[styleStatic, style]}>
      {children}
    </View>
  )
}

const styleStatic = {
  width: '100%',
  flexDirection: 'row',
  padding: 0,
  alignItems: 'flex-start',
  justifyContent: 'flex-start',
}
