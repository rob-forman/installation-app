import React from 'react'
import { View, TextInput, StyleSheet } from 'react-native'
import { ButtonScan } from './Buttons'
import ErrorMessage from './ErrorMessage'
import { COLOR_RED, COLOR_DARK_GREY, COLOR_LIGHT_GREY } from './constants'
import { BASELINE } from '../ui/constants'

export default function TypeOrScan({ field, placeholder, onChange, onScanPress, helper }) {
  const underlineColor = field.error ? COLOR_RED : COLOR_DARK_GREY

  return (
    <View style={styles.container}>
      <View style={styles.text}>
        <TextInput
          onChangeText={onChange}
          style={styles.textInput}
          value={field.value}
          ref={ref => this.input = ref}
          underlineColorAndroid={underlineColor}
          placeholder={placeholder}
          returnKeyType="next"
        />
        <ErrorMessage field={field} />
      </View>
      {onScanPress &&
        <View>
          <ButtonScan onPress={onScanPress} size={32} bordered />
        </View>
      }
    </View>
  )
}

const styles = {
  container: {
    height: 100,
    width: '100%',
    flexDirection: 'row',
    padding: 0,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  text: {
    flex: 1,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#dedede',
    paddingLeft: 17,
  },
  textInput: {
    height: 50,
    fontSize: 16,
  },
}
