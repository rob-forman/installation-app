import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Picker, Form, Label } from 'native-base'
import {
  Text,
  StyleSheet,
  Platform,
} from 'react-native'
import * as UI from '../ui/constants'

export default class Select extends Component {
  constructor(props) {
    super(props)
  }

  render () {
    const {
      error,
      children,
      label,
      placeholder,
      selectedValue,
      onValueChange,
      marginTop = 0,
      marginBottom = 0,
      ...rest
    } = this.props

    if (Platform.OS == 'android' && children.length && !selectedValue) {
      children.unshift(<Option label={this.props.placeholder} value={null} key="null" />)
    }

    return (
      <Form style={{
        marginTop: marginTop * UI.BASELINE,
        marginBottom: marginBottom * UI.BASELINE,
      }}>
        { label && <Label style={styles.label}>{label}</Label> }
        <Picker
          mode="dropdown"
          placeholder={placeholder}
          iosHeader={placeholder}
          selectedValue={selectedValue}
          onValueChange={onValueChange}
          style={ Platform.OS == 'ios' ? styles.picker : {} }
          {...rest}
        >
          {children}
        </Picker>
        { error && <Text style={Style.errorText}>{error}</Text> }
      </Form>
    )
  }
}

Select.propTypes = {
  error: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  selectedValue: PropTypes.any,
  onValueChange: PropTypes.func.isRequired,
  marginTop: PropTypes.number,
  marginBottom: PropTypes.number,
}

export const Option = Picker.Item

const styles = {
  picker: {
    width: '100%',
    height: UI.BASELINE,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: UI.BORDER_RADIUS,
    borderColor: '#dedede',
  },
  label: {
    marginBottom: UI.BASELINE / 4,
    fontSize: 13,
    color: '#333333',
  }
}

const Style = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonText: {
    height: UI.BASELINE - 2,
    lineHeight: UI.BASELINE - 2,
    borderRadius: UI.BORDER_RADIUS,
    marginLeft: UI.SPACING * 2,
    marginRight: UI.SPACING * 2,
    color: UI.COLOR_GREY,
    fontSize: 16,
    fontWeight: 'bold',
  },
  errorText: {
    marginTop: 1,
    paddingLeft: UI.SPACING * 2,
    paddingRight: UI.SPACING * 2,
    color: UI.COLOR_RED,
    fontSize: 12,
    fontWeight: 'bold',
    height: UI.BASELINE / 2,
    lineHeight: UI.BASELINE / 2,
  }
})
