import React from 'react'
import { Icon, Text, Button as Btn } from 'native-base'
import { COLOR_WHITE } from './constants'
import { COLOR_BLUE } from './constants'
import { COLOR_DARK_GREY } from './constants'
import { COLOR_GREEN } from './constants'

export const Button = ({ size, onPress, children, icon, ...rest }) =>
<Btn info iconRight={!!icon} onPress={onPress} {...rest} style={styles.btn_1} >
{ children &&
    <Text style={styles.text}>{children}</Text>
}
{ !!icon &&
    <Icon
    name={icon}
    style={{ fontSize: 26, color: COLOR_WHITE }}
    />
}
</Btn>

export const ButtonGuide = ({ size, onPress, children, icon, ...rest }) =>
<Btn info iconRight={!!icon} onPress={onPress} {...rest} style={styles.btn_guide} >
{ children &&
    <Text style={styles.text_blue}>{children}</Text>
}
{ !!icon &&
    <Icon
    name={icon}
    style={{ fontSize: 26, color: COLOR_BLUE }}
    />
}
</Btn>

export const ButtonPrev = ({ size, onPress, children, ...rest }) =>
<Btn info iconLeft onPress={onPress} {...rest} style={{ backgroundColor: COLOR_WHITE }} >
<Icon
android="md-arrow-dropleft"
ios="ios-arrow-dropleft"
style={{...styles.left, fontSize: 26, color: COLOR_BLUE }}
/>
{ children &&
    <Text style={styles.text_blue}>{children}</Text>
}
</Btn>

export const ButtonNext = ({ size, onPress, children, ...rest }) =>
<Btn info iconRight onPress={onPress} {...rest} style={{ backgroundColor: COLOR_WHITE }} >
{ children &&
    <Text style={styles.text_blue}>{children}</Text>
}
<Icon
android="md-arrow-dropright"
ios="ios-arrow-dropright"
style={{ fontSize: 26, color: COLOR_BLUE }}
/>
</Btn>

export const ButtonDone = ({ size, onPress, children, ...rest }) =>
<Btn info iconRight onPress={onPress} {...rest} style={{ backgroundColor: COLOR_GREEN }}>
{ children &&
    <Text style={styles.text}>{children}</Text>
}
<Icon
android="md-checkmark"
ios="ios-checkmark"
style={{ fontSize: 30, color: COLOR_WHITE }}
/>
</Btn>

export const ButtonScan = ({ size, onPress, ...rest }) =>
<Btn info onPress={onPress} {...rest} style={styles.btn_scan}  >
<Icon
name="barcode"
style={Object.assign({ fontSize: size, color: COLOR_WHITE }, styles.noMargin)}
/>
</Btn>

const styles = {
left: {
marginLeft: 15,
},
text: {
fontSize: 16,
fontWeight: 'normal',
},
noMargin: {
marginLeft: 0,
marginRight: 0,
},
textAlignLeft: {
textAlign: 'left',
},
text_blue: {
color: COLOR_BLUE,
fontSize: 16,
fontWeight: 'normal',
},
btn_1: {
backgroundColor: COLOR_BLUE,
width: '100%',
height: 56,
borderRadius: 2,
marginBottom: 10,
},
btn_guide: {
backgroundColor: COLOR_WHITE,
},
btn_scan: {
backgroundColor: COLOR_DARK_GREY,
borderColor: COLOR_DARK_GREY,
}
}

