export const BASELINE = 40
export const SPACING = 4
export const BORDER_RADIUS = 4

export const COLOR_BLUE = '#007dd7'
export const COLOR_GREEN = '#4cd66d'
export const COLOR_LIGHTBLUE = '#b2ddff'
export const COLOR_LIGHT_GREY = '#dedede'
export const COLOR_GREY = '#85868a'
export const COLOR_DARK_GREY = '#666666'
export const COLOR_WHITE = '#fff'
export const COLOR_RED = '#f93e5a'
export const COLOR_LIGHT_GREY_BLUE = '#e8ebf0'

