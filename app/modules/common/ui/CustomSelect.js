import React, { Component } from 'react'
import { View, TextInput } from 'react-native'
import { Label } from 'native-base'
import Select, { Option } from './Select'
import ErrorMessage from './ErrorMessage'
import { BASELINE } from '../ui/constants'

class CustomSelect extends Component {
  constructor(props) {
    super(props)

    if (props.options.includes(props.field.value)) {
      this.state = {
        activeSelect: props.field.value
      }
    } else if (!props.field.value) {
      this.state = {
        activeSelect: ''
      }
    } else {
      this.state = {
        activeSelect: props.otherOption
      }
    }
  }

  onSelectChanged = (activeSelect) => {
    this.setState({
      activeSelect
    })
    if (this.props.otherOption !== activeSelect) {
      this.props.onChange(activeSelect)
    } else {
      // clearing the value for textfield
      this.props.onChange('')
    }
  }

  isOthersSelected(){
    return this.state.activeSelect === this.props.otherOption
  }

  render() {
    const {
      options,
      onChange,
      field,
      placeholder,
      textPlaceholder,
      label,
      marginBottom = 0,
      marginTop = 0
    } = this.props

    return (
      <View style={{
        marginTop: marginTop * BASELINE,
        marginBottom: marginBottom * BASELINE,
      }}>
        { label && <Label style={styles.label}>{label}</Label> }
        <View style={styles.row}>
          <View style={styles.select}>
            <Select
              placeholder={placeholder}
              selectedValue={this.state.activeSelect}
              onValueChange={this.onSelectChanged}
            >
              { options.map(a =>
                <Option key={a} label={a} value={a} />
              )}
            </Select>
          </View>
          {this.isOthersSelected() &&
            <View style={styles.text}>
              <TextInput
                onChangeText={onChange}
                value={field.value}
                style={styles.input}
                ref={ref => this.input = ref}
                placeholder={textPlaceholder}
                returnKeyType="next"
              />
            </View>
          }
          </View>
        <ErrorMessage field={field} />
      </View>
    )
  }
}

export default CustomSelect

const styles = {
  label: {
    marginBottom: BASELINE / 4,
    fontSize: 13,
    color: '#333333',
  },
  row: {
    flexDirection: 'row',
  },
  select: {
    flex: 2,
  },
  text: {
    flex: 3,
  },
  input: {
    height: 50,
    paddingLeft: BASELINE / 2,
  },
}
