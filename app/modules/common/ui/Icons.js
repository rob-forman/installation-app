import React from 'react'
import { Icon } from 'native-base'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { COLOR_RED, COLOR_GREEN, COLOR_BLUE } from './constants'
import { SENSOR_TYPES } from '../constants'

export const IconAdd = ({ size, onPress }) =>
  <Icon
    android="md-add-circle"
    ios="ios-add-circle"
    style={{ fontSize: size, color: COLOR_BLUE }}
    onPress={onPress}
  />

export const IconRemove = ({ size, onPress }) =>
  <Icon
    android="md-close-circle"
    ios="ios-close-circle"
    style={{ fontSize: size, color: COLOR_BLUE }}
    onPress={onPress}
  />

export const IconRefresh = ({ size, onPress }) =>
  <Icon
    android="md-refresh-circle"
    ios="ios-refresh-circle"
    style={{ fontSize: size, color: COLOR_BLUE }}
    onPress={onPress}
  />

export const SensorIcon = ({ type, size, color }) =>
  <MdIcon
    name={SENSOR_TYPES[type]}
    size={size}
    color={color}
  />

export const Checkmark = ({ value, size }) => value ?
  <Icon
    android="md-checkmark"
    ios="ios-checkmark"
    style={{ color: COLOR_GREEN, fontSize: size }}
  />
  :
  <Icon
    android="md-close"
    ios="ios-close"
    style={{ color: COLOR_RED, fontSize: size }}
  />
