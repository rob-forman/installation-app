import React from 'react'
import { Text } from 'react-native'
import * as UI from '../ui/constants'

export default function Label ({children}) {
  return (
    <Text style={labelStyle}>
      {children}
    </Text>
  )
}

const labelStyle = {
  marginBottom: UI.SPACING * 2,
  lineHeight: UI.BASELINE / 2,
  fontSize: 16,
  fontWeight: '900',
}
