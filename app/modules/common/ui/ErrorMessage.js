import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, StyleSheet } from 'react-native'
import * as UI from '../ui/constants'

export default class ErrorMessage extends Component {
  constructor(props) {
    super(props)
  }

  render () {
    const {
      children,
      field = {},
      style = {},
    } = this.props
    const message = field.error || children

    return message ?
      <Text style={[Style.errorText, style]}>
        {message}
      </Text> :
      null
  }
}

ErrorMessage.propTypes = {
  field: PropTypes.object,
  style: PropTypes.object,
}

const Style = StyleSheet.create({
  errorText: {
    marginTop: 1,
    paddingLeft: UI.SPACING * 2,
    paddingRight: UI.SPACING * 2,
    color: UI.COLOR_RED,
    fontSize: 12,
    fontWeight: 'bold',
    height: UI.BASELINE / 2,
    lineHeight: UI.BASELINE / 2,
  }
})
