import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { List, ListItem, Left, Body, Text as BaseText } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Checkmark } from '../ui/Icons'
import * as UI from '../ui/constants'
import { COLOR_LIGHT_GREY_BLUE } from './constants'

import I18n from '../../installs/locale'

export default class Breadcrumbs extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      isDetailsOpened: false
    }
  }

  toggleDetails = () => {
    this.setState(prevState => ({ isDetailsOpened: !prevState.isDetailsOpened }))
  }

  renderIsCompletedIcon = (isCompleted) => (isCompleted ?
    <Icon
      name="check-circle"
      size={12}
      style={{ color: UI.COLOR_BLUE }}
    />
    : null)

  render() {
    const { active, values } = this.props
    return (
      <View>
        <View style={styles.container}>
          {this.props.items.map((item, idx) =>
            <View key={item.id} style={styles.item}>
              <View>
                <Text style={styles.label}>
                  {this.renderIsCompletedIcon(item.isCompleted)}
                  <Text> </Text>
                  <Text style={ idx == active && { color: UI.COLOR_BLUE } }>
                    {idx == active ? item.name : item.id}
                  </Text>
                </Text>
              </View>
              {idx != this.props.items.length - 1 &&
                <View style={styles.divider}>
                  <Icon
                    name="chevron-right"
                    size={14}
                    style={styles.label}
                  />
                </View>
              }
            </View>
          )}
          <View style={buttonStyles}>
            <Icon
              name="information"
              size={24}
              color={UI.COLOR_BLUE}
              onPress={this.toggleDetails}
            />
          </View>
        </View>

        {this.state.isDetailsOpened &&
          <List style={listStyles}>
            <ListItem icon>
              <Left>
            <Checkmark value={values.company} size={24}/>
              </Left>
              <Body>
            <BaseText style={baseText}>{I18n.t('installs.company')}</BaseText>
            <BaseText note style={note}>{values.company}</BaseText>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Checkmark value={values.assetId} />
              </Left>
              <Body>
                <BaseText style={baseText}>{I18n.t('installs.assetId')}</BaseText>
                <BaseText note style={note}>{values.assetId}</BaseText>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Checkmark value={values.commId} />
              </Left>
              <Body>
                <BaseText style={baseText}>{I18n.t('installs.commId')}</BaseText>
                <BaseText note style={note}>{values.commId}</BaseText>
              </Body>
            </ListItem>
            <ListItem icon last>
              <Left>
                <BaseText note>{values.sensors}</BaseText>
              </Left>
              <Body>
                <BaseText style={baseText}>{I18n.t('installs.sensorsAssigned')}</BaseText>
              </Body>
            </ListItem>
          </List>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: UI.COLOR_LIGHT_GREY_BLUE,
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    fontSize: 12,
    fontWeight: 'normal',
    color: UI.COLOR_DARK_GREY,
  },
  divider: {
    marginLeft: 5,
    marginRight: 5,
  }
})

const baseText = {
fontSize: 11,
color: '#333',
}

const note = {
fontSize: 15,
}

const listStyles = {
backgroundColor: '#fff',
}

const buttonStyles = {
  marginLeft: 'auto',
}
