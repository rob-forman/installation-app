import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
//import { bindActionCreators } from 'redux'
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  Linking,
  Platform,
  TouchableHighlight,
} from 'react-native'
import { Thumbnail } from 'native-base'
import { isFieldValid } from '../../tools/Validation'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export const Logo = () => (
  <Image
    source={require('../../../assets/img/globetracker_logo.png')}
    style={Platform.select({
      ios: { margin: 10, width: 160, height: 40 },
      android: { margin: 10, width: 190, height: 48 },
    })}
  />
)

export const LogoDrawer = () => (
                           <Image
                           source={require('../../../assets/img/globetracker_logo.png')}
                           style={Platform.select({
                                                  ios: { margin: 20, marginTop: 50, width: 160, height: 40 },
                                                  android: { margin: 20, marginTop: 50, width: 190, height: 48 },
                                                  })}
                           />
                           )

export const LogoIcon = () => (
<Icon
name="menu"
                               style={{color: '#ffffff',
    margin: 4, fontSize: 30,
      ...Platform.select({
        ios: { width: 32, height: 32 },
        android: { width: 40, height: 40 },
      }),
    }}
  />
)

export const DrawerHeaderLeftComponent = props => (
  <View style={{ flex: 1, flexDirection: 'row' }}>
    <TouchableOpacity
      style={{ padding: 5 }}
      onPress={() => {
        props.navigation.navigate('DrawerOpen')
      }}
    >
      <LogoIcon />
    </TouchableOpacity>
  </View>
)

export const DrawerHeaderLeft = connect(state => ({}), dispatch => ({}))(
  DrawerHeaderLeftComponent,
)

export const Link = ({ title, url }) => (
  <TouchableOpacity
    style={CommonStyle.link}
    onPress={() => {
      Linking.canOpenURL(url).then(canOpen => canOpen && Linking.openURL(url))
    }}
  >
    <Text style={CommonStyle.linkText}>{title}</Text>
  </TouchableOpacity>
)

export const PrimaryButton = ({ title, onPress, disabled}) => (
  <TouchableOpacity style={CommonStyle.primaryButton} disabled={disabled} onPress={onPress}>
    <Text style={CommonStyle.primaryButtonText}>{title}</Text>
  </TouchableOpacity>
)

export const SecondaryButton = ({
  title,
  style = {},
  titleStyle = {},
  onPress,
}) => (
  <TouchableOpacity
    style={[CommonStyle.secondaryButton, ...style]}
    onPress={onPress}
  >
    <Text style={[CommonStyle.secondaryButtonText, ...titleStyle]}>
      {title}
    </Text>
  </TouchableOpacity>
)

export const IconButton = ({
  name,
  size = 16,
  color = '#077ad1',
  backgroundColor = '#ffffff',
  onPress,
}) => (
  <TouchableOpacity
    style={[
      CommonStyle.iconButton,
      {
        backgroundColor: backgroundColor,
      },
    ]}
    onPress={onPress}
  >
    <Icon
      style={{ height: '100%', width: '100%', textAlign: 'center' }}
      name={name}
      size={size}
      color={color}
    />
  </TouchableOpacity>
)

export const IconTextButton = ({
  name,
  text = '',
  size = 16,
  color = '#077ad1',
  backgroundColor = '#ffffff',
  onPress,
}) => (
  <TouchableOpacity
    style={[CommonStyle.iconTextButton, { backgroundColor: backgroundColor }]}
    onPress={onPress}
  >
    <Icon
      style={{ height: '100%', width: '30%', textAlign: 'right', marginRight: 5 }}
      name={name}
      size={size*1.4}
      color={color}
    />
    <Text
      style={{
        height: '100%',
        width: '70%',
        margin: 0,
        fontSize: size,
        color: color,
        textAlign: 'left',
      }}
    >
      {text}
    </Text>
  </TouchableOpacity>
)

export const DashButton = ({
  title,
  image,
  onPress,
  style = {},
  iconStyle = {},
  iconName = '',
  iconSize = 26,
  textStyle = {},
  disabled = false,
  children
}) => (
  <TouchableOpacity
    style={[CommonStyle.dashButton, ...style]}
    onPress={onPress}
    disabled={disabled}
  >
       <View style={{ height: '100%', flex: 1, flexDirection: 'row' }}>
       <Image
       source={image}
       style={{
       ...Platform.select({
           ios: { marginRight:18, width: 40, height: 40 },
           android: { marginRight:18, width: 40, height: 40 },
           }),
       }}
       />
      <Text style={[CommonStyle.dashButtonText, ...textStyle]}>{title}</Text>
      {children}
    </View>
  </TouchableOpacity>
)

export const ActionButton = ({
  title = '',
  isPending = false,
  disabled = false,
  onPress = () => {},
}) => (isPending) ?
  <View style={{ flex: 1, width: '100%', marginTop: 20 }}>
    <ActivityIndicator size="large" color="#007dd7" animating={isPending} />
  </View>
  :
  <View style={{ flex: 1, width: '100%', marginTop: 20 }}>
    <PrimaryButton onPress={onPress} title={title} disabled={disabled}/>
  </View>

export const BackButton = ({
  disabled = false,
  onPress
}) => <ActionButton title="Back" disabled={disabled} onPress={onPress}/>

export const NextButton = ({
  disabled = false,
  onPress = () => {}
}) =>  <ActionButton title="Next" disabled={disabled} onPress={onPress} />

export const ThumbnailView = ({
  image,
  text,
  onPress
}) =>
  (image && image.uri !== null) &&
    <TouchableHighlight
      onPress={onPress(image)}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <Thumbnail square large source={image} />
        {!!text && (<Text>{text}</Text>)}
      </View>
    </TouchableHighlight>

export class TextInputField extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let {
      inputName = 'input',
      field,
      helperText = '',
      underlineColorAndroid = '#666666',
    } = this.props
    let style = CommonStyle.helperText
    let message = helperText
    if (!isFieldValid(field)) {
      underlineColorAndroid = '#ff8888'
      style = CommonStyle.errorText
      message = field.error + ' ' + helperText
    }
    return (
      <View style={{ height: 64 }}>
        <TextInput
          {...this.props}
          style={{ height: 40 }}
          value={field.value}
          ref={ref => this[inputName] = ref}
          underlineColorAndroid={underlineColorAndroid}
        />
        <Text style={[style, { height: 24 }]}>
          {message}
        </Text>
      </View>
    )
  }
}

export class CheckBox extends Component {
  constructor(props) {
    super(props)
    this.state = { checked: props.checked }
    this.styles = StyleSheet.create({
      label: {
        fontSize: 16,
      },
      icon: {
        marginLeft: -10,
      },
    })

    this.onPress = () => {
      let toggledChecked = !this.state.checked
      this.setState({checked: toggledChecked})
      this.props.onChange(toggledChecked)

    }
  }

  render() {
    const iconName = this.state.checked ? this.props.checkedIconName : this.props.uncheckedIconName
    return (
      <Icon.Button
        {...this.props}
        name={iconName}
        size={this.props.size}
        backgroundColor={this.props.backgroundColor}
        color={this.props.color}
        iconStyle={[this.styles.icon, this.props.iconStyle, this.props.checked && this.props.checkedIconStyle]}
        onPress={this.onPress}
        activeOpacity={this.props.activeOpacity}
        underlayColor={this.props.underlayColor}
        borderRadius={this.props.borderRadius}
      >
        <Text
          style={[this.styles.label, this.props.labelStyle]}
        >
          {this.props.label}
        </Text>
      </Icon.Button>
    )
  }
}

CheckBox.propTypes = {
  size: PropTypes.number,
  checked: PropTypes.bool,
  label: PropTypes.string,
  labelStyle: Text.propTypes.style,
  iconStyle: Text.propTypes.style,
  checkedIconStyle: Text.propTypes.style,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  onPress: PropTypes.func,
  underlayColor: PropTypes.string,
  activeOpacity: PropTypes.number,
  borderRadius: PropTypes.number,
  uncheckedIconName: PropTypes.string,
  checkedIconName: PropTypes.string,
}

CheckBox.defaultProps = {
  size: 28,
  checked: false,
  labelStyle: {},
  iconStyle: {},
  checkedIconStyle: {},
  color: '#444444',
  backgroundColor: 'rgba(0,0,0,0)',
  underlayColor: 'rgba(0,0,0,0)',
  activeOpacity: 1,
  borderRadius: 5,
  uncheckedIconName: 'checkbox-blank-outline',
  checkedIconName: 'checkbox-marked-outline',
}

export const CommonStyle = StyleSheet.create({
  helperText: {
    padding: 5,
    paddingTop: 0,
    color: '#aaaaaa',
    fontSize: 11,
    width: '100%',
    height: 24,
  },
  errorText: {
    padding: 5,
    paddingTop: 0,
    color: '#ff8888',
    fontSize: 11,
    fontWeight: 'bold',
    width: '100%',
    height: 24,
  },
  textInput: {
    marginTop: 10,
    height: 40,
    width: '100%',
  },
  link: {},
  linkText: {
    color: '#077ad1',
  },
  primaryButton: {
    height: 40,
    margin: 0,
    borderWidth: 0,
    backgroundColor: '#077ad1',
    padding: 10,
    borderRadius: 4,
    elevation: 2,
  },
  primaryButtonText: {
    color: '#ffffff',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  secondaryButton: {
    margin: 10,
    borderWidth: 0,
    backgroundColor: '#ffffff',
    padding: 10,
    borderRadius: 4,
    elevation: 2,
  },
  secondaryButtonText: {
    color: '#4682B4',
  },
  iconButton: {
    margin: 10,
    borderWidth: 0,
    backgroundColor: '#ffffff',
    padding: 5,
    borderRadius: 4,
    elevation: 2,
  },
  iconTextButton: {
    flexDirection: 'row',
    margin: 10,
    marginLeft: 0,
    marginRight: 0,
    borderWidth: 0,
    backgroundColor: '#ffffff',
    padding: 4,
    borderRadius: 4,
    elevation: 2,
  },
  dashButton: {
    height: 100,
    width: '100%',
    marginLeft: 0,
    marginRight: 0,
    marginTop: 1,
    marginBottom: 12,
    backgroundColor: '#ffffff',
    paddingTop: 30,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 2,
    elevation: 2,
    ...Platform.select({
      ios: {
        borderWidth: 1,
        borderColor: '#ffffff',
        borderBottomColor: '#cccccc',
      },
    }),
  },
  dashButtonIcon: {
  },
  dashButtonText: {
    fontSize: 18,
    color: '#333333',
    fontWeight: 'normal',
    marginTop: 10,
  },
})

export const PopupMenuStyle = {
  trigger: {
    triggerText: {},
    triggerOuterWrapper: {},
    triggerWrapper: {},
    triggerTouchable: {
      backgroundColor: '#ffffff',
    },
  },
  options: {
    optionsContainer: {},
    optionsWrapper: {},
    optionWrapper: {},
    optionTouchable: {},
    optionText: {},
  },
  option: {
    optionTouchable: {},
    optionWrapper: {},
    optionText: {
      margin: 10,
      fontSize: 18,
    },
  },
  optionSeparator: {
    optionTouchable: {},
    optionWrapper: {
      borderBottomWidth: 1,
      borderBottomColor: '#eeeeee',
    },
    optionText: {
      margin: 10,
      fontSize: 18,
    },
  },
}
