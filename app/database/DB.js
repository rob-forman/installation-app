import log from '../tools/log'
import SQLite from 'react-native-sqlite-storage'
SQLite.enablePromise(true)

const DB = {}
DB.VERSION = 1 // reflects current state of code
DB.FILE = 'GT_Installs.db'
DB.LOCATION = 'default' // 'default', 'Library', or 'Documents'



DB.getDB = async () =>
  await SQLite.openDatabase({ name: DB.FILE, location: DB.LOCATION })

DB.execute = async (sql, params = []) => {
  try {
    const db = await DB.getDB()
    const rs = await db.executeSql(sql, params)
    return rs[0] //db.executeSql returns [rs], so unwrap
  } catch (e) {
    throw e
  }
}

DB.getOne = async (sql, params = []) => {
  const rs = await DB.execute(sql, params)
  return rs.rows.length > 0 ? rs.rows.item(0) : null
}

DB.getAll = async (sql, params = []) => {
  const rs = await DB.execute(sql, params)
  return rs.rows.length > 0 ? rs.rows.raw() : []
}

DB.debugDumpAll = () => {
  DB.getAll('SELECT * FROM version', [])
    .then(rows => log('DB DEBUG: version', rows))
    .catch(e => log(e))

  DB.getAll('SELECT * FROM app_state', [])
    .then(rows => log('DB DEBUG: app_state', rows))
    .catch(e => log(e))

  DB.getAll('SELECT * FROM users', [])
    .then(rows => log('DB DEBUG: users', rows))
    .catch(e => log(e))

  DB.getAll('SELECT * FROM installs', [])
    .then(rows => log('DB DEBUG: installs', rows))
    .catch(e => log(e))
}

DB.setup = () => {
  return new Promise((resolve, reject) => {
    DB.migrate().then(() => resolve()).catch(e => reject(e))
  })
}

DB.version0to1 = tx => {
  log('DB:Migrate [IN PROGRESS]: Version 0 -> 1 ')

  tx.executeSql(
    'CREATE TABLE version (' +
      'version INTEGER PRIMARY KEY NOT NULL,  ' +
      'created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
  )
  tx.executeSql('INSERT INTO version (version) VALUES (1)')

  tx.executeSql(
    'CREATE TABLE app_state (' +
      'state_key TEXT NOT NULL, ' +
      'state_val TEXT NOT NULL)',
  )

  tx.executeSql(
    'CREATE TABLE users (' +
      'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
      'username TEXT NOT NULL, ' +
      'email TEXT NOT NULL, ' +
      'auth_token TEXT, ' +
      "role TEXT NOT NULL DEFAULT 'user', " +
      'created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
  )

  log('DB:Migrate    [COMPLETE]: Version 0 -> 1 ')
}

DB.MIGRATIONS = [
  { from: 0, to: 1, fn: DB.version0to1 },
  //{ from: 1, to: 2, fn: DB.version1to2 },
]

DB.migrator = version => {
  log('DB VERSION: ' + version)
  if (version == DB.VERSION) {
    return
  }
  DB.getDB().then(db => {
    for (let m of DB.MIGRATIONS) {
      if (version < DB.VERSION && m.from >= version && m.to <= DB.VERSION) {
        db.transaction(m.fn, e => log(e))
      }
    }
  })
}

DB.migrate = () => {
  return new Promise((resolve, reject) => {
    DB.getOne('SELECT MAX(version) as version FROM version LIMIT 1', [])
      .then(row => {
        DB.migrator(row.version)
        resolve()
      })
      .catch(() => {
        DB.migrator(0)
        resolve()
      })
  })
}

export default DB
