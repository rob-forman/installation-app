export const isNull = x => x === null
export const isBlank = s => {
  return (typeof s === 'undefined') || isNull(s) || (typeof s === 'string' && s.trim() == '')
}

export const isValidEmail = s => {
  return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(s)
}


/* Fields look like:
 * {value: 'myvalue'
 *  error: 'error msg' OR null}
 */
export const isFieldValid = field => field.error == null

export const allFieldsValid = (fields = []) => {
  for (let field of fields) {
    if (!isFieldValid(field)) {
      return false
    }
  }
  return true
}
