
// Reduce a single form field within a state structure like:
//  {form: {fieldOne: {value: 'a', error: null}
//          fieldTwo: {value: 'b', error: null}}}
export const reduceFormField = (fieldName, state, action) => {
  return {
    ...state,
    form: {
      ...state.form,
      [fieldName]: {...state.form[fieldName], ...action}
    }
  }
}
