
const log = (msg, obj = null) => {
  console.log(msg)
  obj && console.log(obj)
}

export default log
