# Globe Tracker Installation App


## Terminology
Asset = container, smallest unit; can be of two types:
 - *dry* (plain steel containnr) 
 - *reefer* (a "giant refrigerator")

Every asset can contain items from multiple clients.

Install = an (asset, client) pair. Must contain:
  - CM (central module = communication unit = "a box")
    - identified by an ID or a barcode
  - sensor(s)
    - also have an ID/barcode

Installation = act of gathering stuff around the asset

  

## Additional documents: 

[Missing features and technical recommendations](https://docs.google.com/document/d/1LqKjSAtKbNiZEdy5Q_32qBwPA0WS3mMNM87f93NoiOY/edit#)

[Official specification, divergent from the codebase](https://github.com/salsita/GlobeTracker/blob/master/docs/install_app_feb_2017_03.pdf)
