#!/bin/bash

set -e

ENV=

case "$CIRCLE_BRANCH" in
  "develop")
    ENV="dev"
    ;;
  "qa")
    ENV="qa"
    ;;
  "master")
    ENV="master"
    ;;
  *)
    ENV="try"
    ;;
esac

echo "export BUILD_ENV=\"$ENV\"" >> ~/.circlerc
