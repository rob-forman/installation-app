#!/bin/bash

set -eu

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $CIRCLE_BUILD_NUM" "ios/GlobeTrackerInstallationsApp/Info.plist"
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $VERSION" "ios/GlobeTrackerInstallationsApp/Info.plist"

sed -i '' "s|versionCode [^ ]*$|versionCode $CIRCLE_BUILD_NUM|g" android/app/build.gradle
sed -i '' "s|versionName [^ ]*$|versionName \"$VERSION\"|g" android/app/build.gradle
