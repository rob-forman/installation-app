#!/bin/bash

set -euo pipefail

CURRENT=sdk-tools-darwin-3859397.zip
cd ~
mkdir .android
cd .android
touch repositories.cfg
curl -s https://dl.google.com/android/repository/$CURRENT > tools.zip
curl -s https://dl.google.com/android/repository/platform-tools-latest-darwin.zip > platform-tools.zip
unzip tools.zip > /dev/null
unzip platform-tools.zip > /dev/null
rm platform-tools.zip tools.zip
echo y | ./tools/bin/sdkmanager "platforms;android-25"
echo y | ./tools/bin/sdkmanager "build-tools;26.0.1"

echo "export PATH=$HOME/.android/tools:$HOME/.android/platform-tools:$PATH" >> ~/.circlerc
echo "export ANDROID_HOME=$HOME/.android" >> ~/.circlerc
