#!/bin/bash

set -euo pipefail

curl -sS \
  -H "X-HockeyAppToken: $HOCKEYAPP_IOS_TOKEN" \
  -F "status=2" \
  -F "notify=0" \
  -F "notes=$( git log --format=%B -n 1 $CIRCLE_SHA1 )" \
  -F "tags=${BUILD_ENV}" \
  -F "commit_sha=${CIRCLE_SHA1}" \
  -F "build_server_url=${CIRCLE_BUILD_URL}" \
  -F "repository_url=${CIRCLE_REPOSITORY_URL}" \
  -F "ipa=@${CIRCLE_ARTIFACTS}/GlobeTrackerInstallationsApp-${CIRCLE_BRANCH}-${CIRCLE_BUILD_NUM}.ipa" \
  https://rink.hockeyapp.net/api/2/apps/"$HOCKEYAPP_IOS_APP_ID"/app_versions/upload | jq .

curl -sS \
  -H "X-HockeyAppToken: $HOCKEYAPP_ANDR_TOKEN" \
  -F "status=2" \
  -F "notify=0" \
  -F "notes=$( git log --format=%B -n 1 $CIRCLE_SHA1 )" \
  -F "tags=${BUILD_ENV}" \
  -F "commit_sha=${CIRCLE_SHA1}" \
  -F "build_server_url=${CIRCLE_BUILD_URL}" \
  -F "repository_url=${CIRCLE_REPOSITORY_URL}" \
  -F "ipa=@${CIRCLE_ARTIFACTS}/GlobeTrackerInstallationsApp-${CIRCLE_BRANCH}-${CIRCLE_BUILD_NUM}.apk" \
  https://rink.hockeyapp.net/api/2/apps/"$HOCKEYAPP_ANDR_APP_ID"/app_versions/upload | jq .
