#!/bin/bash

set -eu

cd android
./gradlew assembleRelease \
  -Pandroid.injected.signing.store.file=/Users/distiller/GlobeTracker/android/keystores/GlobeTracker-release.jks \
  -Pandroid.injected.signing.store.password=$ANDROID_SIGN_PASSWD \
  -Pandroid.injected.signing.key.alias=$ANDROID_KEYSTORE_ALIAS \
  -Pandroid.injected.signing.key.password=$ANDROID_SIGN_PASSWD
