#!/bin/bash

set -eu

cp ios/*.zip ${CIRCLE_ARTIFACTS}/
cp ios/GlobeTrackerInstallationsApp.ipa ${CIRCLE_ARTIFACTS}/GlobeTrackerInstallationsApp-${CIRCLE_BRANCH}-${CIRCLE_BUILD_NUM}.ipa
cp /Users/distiller/GlobeTracker/android/app/build/outputs/apk/app-release.apk ${CIRCLE_ARTIFACTS}/GlobeTrackerInstallationsApp-${CIRCLE_BRANCH}-${CIRCLE_BUILD_NUM}.apk
