#!/bin/bash

set -eo pipefail

PACKAGE_VERSION=$( jq -r '.version' < package.json )
if [[ "$CIRCLE_BRANCH" = 'master' ]] ; then
  echo "export VERSION=${PACKAGE_VERSION}" >> ~/.circlerc
else
  echo "export VERSION=${PACKAGE_VERSION}-${CIRCLE_BRANCH}" >> ~/.circlerc
fi
